# -*- coding: utf-8 -*-

"""
Zadanie 805

Napisz funkcję `classify(bayes, item)`, która wykorzystuje funkcję
`docprob` (zadanie 804) do obliczenia największej wartosci P(C=c|D=d) dla
dokumentu item. Należy zwrócić najbardziej prawdopodobną kategorię.

NAME: classify
PARAMS: NaiveBayes, string
RETURN: string
POINTS: 2
"""

from Task804 import docprob

def classify(bayes, item):
    'zabierzcie stad grzeska ;p'
    odp = ''
    wart = -1e300
    for cat in bayes.class_count.keys():
        tmp = docprob(bayes, item, cat)
        #print tmp, cat, '-----'
        if tmp > wart:
            wart = tmp
            odp = cat
    #print wart, odp
    return odp