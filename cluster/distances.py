# encoding: utf-8

"""Różne metryki."""

def euclidean(vec1, vec2):
    """Odległość między wektorami."""
    return sum([(vec1[idx] - vec2[idx]) ** 2 for idx in range(len(vec1))])\
           ** 0.5


def cosine(vec1, vec2):
    """Odległość cosinusowa między wektorami."""
    return 1 - real_cosine(vec1, vec2)

def real_cosine(vec1, vec2):
    """Oblicza cosinus kąta."""
    norm1 = norm(vec1)
    norm2 = norm(vec2)

    forced_result = check_denominator_factors(norm1, norm2)
    if not forced_result is None:
        return forced_result

    return dot_product(vec1, vec2) / (norm(vec1) * norm(vec2))

def dot_product(vec1, vec2):
    """Oblicza iloczyn wektorów."""
    return sum(vec1[idx] * vec2[idx] for idx in range(len(vec1)))

def norm(vec):
    """Oblicza normę."""
    return sum(val ** 2 for val in vec) ** 0.5



def pearson(vec1, vec2):
    """Odległość między wektorami."""
    return 1 - real_pearson(vec1, vec2)

def real_pearson(vec1, vec2):
    """Współczynnik korelacji Pearsona."""
    vec1_factor = factor(vec1)
    vec2_factor = factor(vec2)

    forced_result = check_denominator_factors(vec1_factor, vec2_factor)
    if not forced_result is None:
        return forced_result

    return (dot_product(vec1, vec2) - sum(vec1) * sum(vec2) / len(vec1))\
           / (vec1_factor * vec2_factor) ** 0.5

def factor(vec):
    """Oblicza współczynnik dla wektora."""
    return sum(val ** 2 for val in vec) - sum(vec) ** 2 / len(vec)

def check_denominator_factors(factor1, factor2):
    """
    Sprawdza czynniki w mianowniku, jeśli oba zerowe zwraca 1,
    jeśli tylko jeden zerowy - 0, jeśli żaden niezerowe - None,
    co oznacza, że można dzielić przez ich iloczyn.
    """

    if factor1 == 0 and factor2 == 0:
        return 1

    if factor1 == 0 or factor2 == 0:
        return 0

    return None
