#!/bin/zsh

PREFIX=$1
TASK=$2

for REPO in $PREFIX-s*
do
INDEX=${REPO#$PREFIX-s}

SOLUTION_FILE="${REPO}/${TASK}.py"

if [[ -r "$SOLUTION_FILE" ]]
then
echo "========================================"
echo "======== $INDEX ========================"
cat "$SOLUTION_FILE"
echo ""
fi

done