# -*- coding: utf-8 -*-

"""
Zadanie 604

Napisz funkcję `separate_data(centroids, data, distance)`, która
zwraca listę K grup identyfikatorów dokumentów (K to liczba
centroidów). Identyfikator dokumentu, to indeks jego wektora w
macierzy `data`. `Centroids` jest listą K centroidów, data jest
macierzą cech, `distance` jest stosowaną funkcją odległości. Funkcja
ma przydzielać dokument do i-tej grupy, jeżeli odległość wektora tego
dokumentu od i-tego centroidu jest mniejsza niż dla pozostałych
centroidów.

NAME: separate_data
PARAMS: list, list, function
RETURN: list
POINTS: 3
"""

def separate_data(centroids, data, distance):
    "asd"
    
    wyn = []
    for asd in range(len(centroids)):
        asd = asd+1-1
        wyn.append([])
    for dat in range(len(data)):
        tmp = [distance(x, data[dat]) for x in centroids]
        tmp2 = tmp.index(min(tmp))
        wyn[tmp2].append(dat)
    
    return wyn
    
