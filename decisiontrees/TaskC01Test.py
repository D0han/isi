# -*- coding: utf-8 -*-

"""
Zadanie C01

Napisz funkcję `create_decision_function(column, value)`, która zwraca
funkcję decyzyjną dla danej wartości `value`. Funkcja decyzyjna jako
argument dostaje wektor, powinna ona porównać element wektora o
numerze `column` z wartością `value`. Dla wartości liczbowych (zarówno
całkowitych, jak i zmiennoprzecinkowych) funkcja decyzyjna powinna
zwracać `True`, gdy element wektora jest większy lub równy `value`,
dla wartości innych typów - tylko wtedy gdy element wektora jest
równy `value`.

NAME: create_decision_function
PARAMS: int, any
RETURN: function
POINTS: 2
"""

import unittest
from TaskC01 import create_decision_function

class TaskC01Test(unittest.TestCase):
    """Testy do zadania TaskC01."""

    def test_integer(self):
        """Test dla progu-liczby całkowitej."""

        decision_function = create_decision_function(2, 362)

        self.assertTrue(decision_function(['yeti', 180, 363, 'yes']))
        self.assertTrue(decision_function(['sasquatch', 210, 362, 'no']))
        self.assertTrue(decision_function(['yeti', 200, 444, 'no']))
        self.assertTrue(decision_function(['bigfoot', 210, 362.5, 'yes']))

        self.assertFalse(decision_function(['yeti', 380, 361, 'yes']))
        self.assertFalse(decision_function(['sasquatch', 210, 361.9, 'no']))
        self.assertFalse(decision_function(['yeti', 200, 0, 'no']))
        self.assertFalse(decision_function(['bigfoot', 210, -10, 'yes']))

    def test_float(self):
        """Test dla progu-liczby zmiennoprzecinkowej."""

        decision_function = create_decision_function(0, -9.5)

        self.assertTrue(decision_function([-9.5, 'foo']))
        self.assertTrue(decision_function([-9, 'bar']))
        self.assertTrue(decision_function([-8.78, 'foo']))
        self.assertTrue(decision_function([0, 'baz']))
        self.assertTrue(decision_function([123.123, 'baz']))

        self.assertFalse(decision_function([-9.6, 'foo']))
        self.assertFalse(decision_function([-12, 'bar']))

    def test_symbol(self):
        """Test dla symbolu."""

        decision_function = create_decision_function(1, 'sasquatch')

        self.assertFalse(decision_function(['sasquatch', 'yeti', 'foo']))
        self.assertFalse(decision_function(['yeti', '', 'foo']))
        self.assertFalse(decision_function(
                ['yeti', 'krokodyl pogryzł wędkarza', 'bar']))

        self.assertTrue(decision_function(['yeti', 'sasquatch', 'foo']))

if __name__ == '__main__':
    unittest.main()
