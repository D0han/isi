#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 104

Napisz metodę `SimpleCrawler.is_html(self, urlobj)`, która sprawdza na
podstawie nagłówka HTTP, czy strona jest dokumentów HTML. Należy
wykorzystać metodę `urlobj.info()`, która zwraca obiekt typu
`httplib.HTTPMessage`. Metody tego obiektu z kolei pozwalają na
sprawdzenie typów dokumentu bez jego pobierania.

NAME: is_html
PARAMS: self, urlobj
RETURN: bool
POINTS: 1
"""

import unittest
from SimpleCrawler import SimpleCrawler
import urllib2

class Task104Test(unittest.TestCase):
    """Testy do zadania 104."""

    def test(self):
        """Test na kilku URL-ach."""
        crawler = SimpleCrawler("test104.db")

        for url, value in [
            ("http://amu.edu.pl/~junczys", True),
            ("http://www.staff.amu.edu.pl/~junczys/images/a/a9/Mjd.jpg",
             False),
            ("http://laboratoria.wmi.amu.edu.pl", True),
            ("http://amu.edu.pl/~junczys/images/9/9e/Mjd2010entropy.pdf",
             False)]:
            print url, value
            self.failUnlessEqual(crawler.is_html(urllib2.urlopen(url)),
                                 value)

if __name__ == '__main__':
    unittest.main()
