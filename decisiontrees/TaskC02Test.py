# -*- coding: utf-8 -*-

"""
Zadanie C02

Napisz funkcję `divide_set(rows, decision_function)`, która dzieli
zbiór wektorów `rows` na dwa podzbiory - pierwszy obejmujący wektory,
dla których `decision_function` zwróciła `True`, drugi - dla których
zwróciła `False`.

NAME: divide_set
PARAMS: list, function
RETURN: list
POINTS: 2
"""

import unittest
from TaskC02 import divide_set

def fake_decision_function(row):
    """Funkcja decyzyjna używana w testach."""
    return row[1] >= 170

class TaskC02Test(unittest.TestCase):
    """Testy do zadania TaskC02."""

    def test(self):
        """Prosty test."""

        rows = [
            ['małpa z Bielawy', 140, 'yes'],
            ['sasquatch', 190, 'no'],
            ['yowie', 200, 'yes'],
            ['krokodyl', 150, 'yes'],
            ['bigfoot', 210, 'no']]

        self.assertEqual(
            divide_set(rows, fake_decision_function),
            [
                [
                    ['sasquatch', 190, 'no'],
                    ['yowie', 200, 'yes'],
                    ['bigfoot', 210, 'no']],
                [
                    ['małpa z Bielawy', 140, 'yes'],
                    ['krokodyl', 150, 'yes']]])


    def test_one_empty(self):
        """Test, w którym jeden z podzbiorów wynikowych będzie pusty."""

        rows = [
            ['foo', 50, 'yes'],
            ['bar', 60, 'no']]

        self.assertEqual(
            divide_set(rows, fake_decision_function),
            [[],
             [['foo', 50, 'yes'],
              ['bar', 60, 'no']]])

if __name__ == '__main__':
    unittest.main()
