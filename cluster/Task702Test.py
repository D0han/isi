# -*- coding: utf-8 -*-

"""
Zadanie 702

Napisz funkcję `closest_pair(clusters, distance, cluster_distance,
distances_cache), która zwraca parę grup o najmniejszej wzajemnej
odległości oraz tę odległość. Dane należy zwrócić w postaci krotki
`(i, j, d)`, gdzie `i` i `j` to indeksy (nie wartość
BiCluster.cluster_id!) odpowiednich grup w liście `clusters`
oraz `d` jest odległością. Parametr `clusters` jest listą
grup typu `Bicluster`, `distance` jest referencją do funkcji
odległości (między obiektami), `cluster_distance` jest funkcją
odległości między skupieniami (zob. funkcję
`average_vector_cluster_distance` w `bicluster.py`), a `distances_cache` jest
pustym słownikiem odległości między parami grup. Ze słownika
`distances_cache` należy korzystać w celu zapobiegania wielokrotnego
obliczania odległości między parami grup. Jego klucze to pary
identyfikatorów grup `(bicluster.cluster_id)`, wartości to
odpowiednie odległości.

NAME: closest_pair
PARAMS: list, function, function dict
RETURN: tuple
POINTS: 4
"""

import unittest
import bicluster
import distances
from Task702 import closest_pair

class Task702Test(unittest.TestCase):
    """Testy do zadania 702."""

    def test(self):
        """Test na jednej macierzy."""

        clusters = [
            bicluster.Bicluster([4.0, 3.0, 0.0, 4.0, 13.0, 1.0], cluster_id=0),
            bicluster.Bicluster([3.0, 0.0, 1.0, 0.0, 2.0, 11.0], cluster_id=1),
            bicluster.Bicluster([6.0, 0.0, 0.0, 7.0, 1.0, 0.0], cluster_id=2),
            bicluster.Bicluster([2.0, 2.0, 1.0, 1.0, 4.0, 5.0], cluster_id=100),
            bicluster.Bicluster([2.0, 1.0, 0.0, 1.0, 1.0, 5.0], cluster_id=123)
        ]

        distances_cache = {}
        left_id, right_id, dist = closest_pair(
            clusters,
            distances.pearson,
            bicluster.average_vector_cluster_distance,
            distances_cache)

        self.assertAlmostEqual(distances_cache[(2, 100)], 1.4472135954999579, 4)

        self.assertEqual(left_id, 1)
        self.assertEqual(right_id, 4)
        self.assertAlmostEqual(dist, 0.04994103671696826, 4)

    def test_forced_cache(self):
        """Test ze sztucznie ustawionym cachem."""

        clusters = [
            bicluster.Bicluster([0.0, 0.0], cluster_id=0),
            bicluster.Bicluster([1.0, 1.0], cluster_id=1),
            bicluster.Bicluster([4.0, 0.0], cluster_id=2)
        ]

        distances_cache = {}

        distances_cache[(0, 1)] = 1000.0
        distances_cache[(1, 2)] = 1500.0

        left_id, right_id, _ = closest_pair(
            clusters,
            distances.euclidean,
            bicluster.average_vector_cluster_distance,
            distances_cache)

        self.assertEqual(left_id, 0)
        self.assertEqual(right_id, 2)

if __name__ == '__main__':
    unittest.main()
