# -*- coding: utf-8 -*-

"""
Zadanie 603

Napisz funkcję `random_centroids(kparam, data)`, która zwraca listę
`kparam` losowo zainicjalizowanych centroidów. Centroid to wektor o
takiej samej długości jak wiersze w macierzy `data`. Wartość na i-tym
miejscu w centroidzie powinna być losową wartością leżącą między
minimum a maksimum i-tej kolumny macierzy `data`. Uwaga: funkcja ma
faktycznie zwracać losowe centroidy, jeśli nie ma będzie zwracać, to
punkty nie będą przyznane nawet jeśli funkcja przejdzie testy.

NAME: random_centroids
PARAMS: int, list
RETURN: list
POINTS: 2
"""

from random import randrange

def random_centroids(kparam, data):
    "nie czytajcie tego kodu, to jest porazka"
    
    wyn = []
    maxi = data[0][:]
    mini = data[0][:]    
    for wier in range(len(data)):
        for wart in range(len(data[wier])):
            if data[wier][wart] < mini[wart]:
                mini[wart] = data[wier][wart]
            if data[wier][wart] > maxi[wart]:
                maxi[wart] = data[wier][wart]
                
    for ilosc in range(kparam):
        ilosc = ilosc +1-1
        wyn.append([])
        for elem in range(len(maxi)):
            wyn[-1].append(randrange(mini[elem], stop=maxi[elem]))
            
    return wyn
