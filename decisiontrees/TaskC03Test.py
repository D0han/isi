# -*- coding: utf-8 -*-

"""
Zadanie C03

Napisz funkcję `divide_by_classes(rows)`, która zlicza ile wektorów należy
do poszczególnych klas, tzn. tworzy słownik, którego kluczami są nazwy
klas, a wartości - liczby wektorów o poszczególnych klasach.
Zakładamy, że nazwa klasy jest zawsze ostatnim elementem wektora.

NAME: divide_by_classes
PARAMS: list
RETURN: dict
POINTS: 2
"""

import unittest
from TaskC03 import divide_by_classes

class TaskC03Test(unittest.TestCase):
    """Testy do zadania TaskC03."""

    def test(self):
        """Prosty test."""

        rows = [
            [4, 180, 'bear'],
            [2, 200, 'bigfoot'],
            [2, 190, 'bigfoot'],
            [2, 170, 'human'],
            [2, 160, 'bear'],
            [0, 150, 'crocodile']]

        self.assertEqual(
            divide_by_classes(rows),
            {'bear': 2, 'bigfoot': 2, 'human': 1, 'crocodile': 1})

if __name__ == '__main__':
    unittest.main()
