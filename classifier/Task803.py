# -*- coding: utf-8 -*-

"""
Zadanie 803

Napisz funkcję `catprob(bayes, category)`, która oblicza logarytm
prawodpodobieństwa P(C=c). Prawdopodobieństwo to obliczamy dzieląc
liczbę wystąpień danej kategorii category przez sumę występowań
wszystkich kategorii. Jeżeli kategoria nie istnieje należy zwrócić
liczbę -1e300 (liczba możliwie bliska log(0) = -inf)

NAME: catprob
PARAMS: NaiveBayes, string
RETURN: float
POINTS: 2
"""

from math import log

def catprob(bayes, category):
    'blablablabla'
    if category not in bayes.class_count:
        return -1e300
    return log( float(bayes.class_count[category]) /\
            sum([x for x in bayes.class_count.values()]))