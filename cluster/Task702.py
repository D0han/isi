# -*- coding: utf-8 -*-

"""
Zadanie 702

Napisz funkcję `closest_pair(clusters, distance, cluster_distance,
distances_cache), która zwraca parę grup o najmniejszej wzajemnej
odległości oraz tę odległość. Dane należy zwrócić w postaci krotki
`(i, j, d)`, gdzie `i` i `j` to indeksy (nie wartość
BiCluster.cluster_id!) odpowiednich grup w liście `clusters`
oraz `d` jest odległością. Parametr `clusters` jest listą
grup typu `Bicluster`, `distance` jest referencją do funkcji
odległości (między obiektami), `cluster_distance` jest funkcją
odległości między skupieniami (zob. funkcję
`average_vector_cluster_distance` w `bicluster.py`), a `distances_cache` jest
pustym słownikiem odległości między parami grup. Ze słownika
`distances_cache` należy korzystać w celu zapobiegania wielokrotnego
obliczania odległości między parami grup. Jego klucze to pary
identyfikatorów grup `(bicluster.cluster_id)`, wartości to
odpowiednie odległości.

NAME: closest_pair
PARAMS: list, function, function dict
RETURN: tuple
POINTS: 4
"""

def closest_pair(clusters, distance, cluster_distance, distances_cache):
    "bilard jest spoko"
    wyn = {}
    for klast1 in clusters:
        for klast2 in clusters:
            if klast1 == klast2:
                break
            if klast1.cluster_id < klast2.cluster_id:
                indeks = (klast1.cluster_id, klast2.cluster_id)
            else:
                indeks = (klast2.cluster_id, klast1.cluster_id)
            if indeks not in distances_cache:
                distances_cache[indeks] = cluster_distance(klast1,
                                        klast2, distance)
            #print indeks, distances_cache[indeks]
            wyn[distances_cache[indeks]] = (clusters.index(klast1),
                                        clusters.index(klast2))
    
    tmp = min(wyn.keys())
    return (wyn[tmp][1], wyn[tmp][0], tmp)
    