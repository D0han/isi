#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 103

Napisz metodę `SimpleCrawler.inject_urls(self, url_list, purge=False)`, która dodaje
każdy adres (w postaci łańcucha znakowego) z listy `url_list` jako klucz do słownika
`self.link_db` z przypisaną mu wartością `0`, o ile taki klucz już nie istnieje. Jeśli
parametr `purge` (domyślnie `False`) ma wartość `True`, to należy przed dodaniem adresów
wyczyścić słownik `self.link_db`.

NAME: inject_urls
PARAMS: self, list, bool
RETURN: None
POINTS: 2
"""

import unittest
from SimpleCrawler import SimpleCrawler

class Task103Test(unittest.TestCase):
    """Testy do zadania 103."""

    def test(self):
        """Testy na stronie MJD."""
        crawler = SimpleCrawler("test103.db")

        url_list_1 = [
            "http://www.google.pl",
            "http://www.google.pl",
            "http://www.google.pl",
            "http://www.wp.pl",
            "http://www.wp.pl",
            "http://docs.python.org/2/library/anydbm.html#anydbm.open"
        ]

        ref_link_db_1 = {
            "http://www.google.pl" : 0,
            "http://www.wp.pl" : 0,
            "http://docs.python.org/2/library/anydbm.html#anydbm.open" : 0
        }

        crawler.inject_urls(url_list_1, True)
        self.failUnlessEqual(ref_link_db_1, crawler.link_db)

        crawler.link_db["http://www.google.pl"] = 1

        url_list_2 = [
            "http://www.google.pl",
            "http://www.wyborcza.pl"
        ]

        ref_link_db_2 = {
            "http://www.google.pl" : 1,
            "http://www.wp.pl" : 0,
            "http://docs.python.org/2/library/anydbm.html#anydbm.open" : 0,
            "http://www.wyborcza.pl" : 0
        }

        crawler.inject_urls(url_list_2, False)
        self.failUnlessEqual(ref_link_db_2, crawler.link_db)

        url_list_3 = [
            "http://www.kozaczek.pl",
            "http://www.wmi.amu.edu.pl",
            "http://www.google.pl"
        ]

        ref_link_db_3 = {
            "http://www.kozaczek.pl" : 0,
            "http://www.wmi.amu.edu.pl" : 0,
            "http://www.google.pl" : 0
        }

        crawler.inject_urls(url_list_3, True)
        self.failUnlessEqual(ref_link_db_3, crawler.link_db)

if __name__ == '__main__':
    unittest.main()
