#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 107

Napisz metodę `SimpleCrawler.crawl(self, depth=3)`, która wykonuje
`depth` iteracji "crawlingu" i po każdej iteracji dodaje linki do bazy
linków. Należy wykorzystać funkcje `self.crawl_one_level()` oraz
`self.inject_urls(urls, purge)`. Test jest napisany tak, że nie jest
wymagana wcześniejsza implementacja wykorzystanych funkcji.

NAME: crawl
PARAMS: self, depth
RETURN: None
POINTS: 1
"""

import unittest
from SimpleCrawler import SimpleCrawler
import urllib2
import random

class SimpleCrawlerForTest(SimpleCrawler):
    def __init__(self):
        self.count = 0;
        self.list = []

    def __del__(self):
        pass

    def crawl_one_level(self):
        self.count += 1
        return set(["http://%d" % self.count])

    def inject_urls(self, urls, purge=False):
        self.list.append([urls, set(["http://%d" % self.count])])


class Task107Test(unittest.TestCase):
    """Testy do zadania 107."""

    def test(self):
        """Test na kilku URL-ach."""
        crawler = SimpleCrawlerForTest()

        depthTest = random.randint(1, 10)
        crawler.crawl(depth=depthTest)

        # test przez obfuskację :)
        randlist = [
            [set(["http://%d" % i]), set(["http://%d" % i])]
            for i in range(1, depthTest+1)]

        self.failUnlessEqual(crawler.list, randlist)


if __name__ == '__main__':
    unittest.main()
