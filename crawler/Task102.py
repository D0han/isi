# -*- coding: utf-8 -*-
"""Rozwiązanie zadania 102."""

def penultimate(lista, otherwise):
    """Zwraca przedostatni element listy"""
    if len(lista) > 1:
        return lista[-2]
    else:
        return otherwise
