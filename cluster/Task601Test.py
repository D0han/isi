# -*- coding: utf-8 -*-

"""
Zadanie 601

Napisz funkcję `euclidean(vec1, vec2)`, obliczającą odległość
euklidesową dla dwóch podanych wektorów.

NAME: euclidean
PARAMS: list, list
RETURN: float
POINTS: 1
"""

import unittest
from Task601 import euclidean

class Task601Test(unittest.TestCase):
    """Testy do zadania 601."""

    def test(self):
        """Test seryjny."""
        for vec1, vec2, distance in [
            ([1.4, 2, 3, 4, 5, 6], [1.4, 2, 3, 4, 5, 6], 0.0),
            ([1.4, 2, 3, 4, 5, 6], [1, 2, 3.3, 4, 5.3, 6], 0.5831),
            ([1, -2.3, 3.0, -4, 5, 6], [6, 2, 1, 2, -1, -3], 14.1594),
            ([0, -12, 0, -23], [-1.4, 3.4, -11.2, 2.3], 31.6962),
            ]:
            self.assertAlmostEqual(euclidean(vec1, vec2), distance, 3)

if __name__ == '__main__':
    unittest.main()
