# -*- coding: utf-8 -*-

"""
Zadanie 706

Napisz funkcję `hcluster(data, distance=distances.pearson,
cluster_distance=average_vector_cluster_distance)` implemetującą
algorytm grupowania hierarchicznego. Argumentami są lista wektorów
(takich jak dla zdania Task701) oraz dwie funkcje odległości
(wykorzystane w zadaniu Task702). Wyjściem funkcji jest pojedynczy
obiekt typu BiCluster, czyli korzeń dendogramu. Wykorzystaj
następujęce elementy i złóż z nich funkcę `hcluster`: from bicluster
import Bicluster, average_vector_cluster_distance; import distances;
from Task701 import initialize_clusters; from Task702 import
closest_pair; from Task703 import average_vectors.

NAME: hcluster
PARAMS: list, function, function
RETURN: BiCluster
POINTS: 4
"""

import unittest

import distances

from bicluster import average_vector_cluster_distance
from Task705 import single_linkage_cluster_distance
from Task706 import hcluster

def recurse(cluster):
    """Konwertuje dendogram na rekursywna listę identyfikatorów"""
    if(not cluster.left and not cluster.right):
        return cluster.cluster_id
    return [recurse(cluster.left), recurse(cluster.right)]

class Task706Test(unittest.TestCase):
    """Testy do zadania 705."""

    def test(self):
        """Test na jednej macierzy."""

        matrix = [
            [4.0, 3.0, 0.0, 4.0, 13.0, 1.0],
            [3.0, 0.0, 1.0, 0.0, 2.0, 11.0],
            [6.0, 0.0, 0.0, 7.0, 1.0, 0.0],
            [2.0, 2.0, 1.0, 1.0, 4.0, 5.0],
            [2.0, 1.0, 0.0, 1.0, 1.0, 5.0]
        ]

        self.assertEqual(
            [[3, [1, 4]], [0, 2]],
            recurse(hcluster(matrix,
                             distances.pearson,
                             average_vector_cluster_distance)))
        self.assertEqual(
            [0, [2, [1, [3, 4]]]],
            recurse(hcluster(matrix,
                             distances.euclidean,
                             average_vector_cluster_distance)))

        self.assertEqual(
            [2, [0, [3, [1, 4]]]],
            recurse(hcluster(matrix,
                             distances.cosine,
                             average_vector_cluster_distance)))

        self.assertEqual(
            [2, [0, [3, [1, 4]]]],
            recurse(hcluster(matrix,
                             distances.pearson,
                             single_linkage_cluster_distance)))
        self.assertEqual(
            [0, [2, [1, [3, 4]]]],
            recurse(hcluster(matrix,
                             distances.euclidean,
                             single_linkage_cluster_distance)))

        self.assertEqual(
            [2, [0, [3, [1, 4]]]],
            recurse(hcluster(matrix,
                             distances.cosine,
                             single_linkage_cluster_distance)))

if __name__ == '__main__':
    unittest.main()
