# -*- coding: utf-8 -*-

"""
Zadanie 801

Napisz funkcję `train(bayes, item, cat)`, która zlicza, ile razy pojawiła
się dana kategoria `cat` oraz, ile razy cechy dokumentu `item`
współwystępowały z daną categorią. Funkcja korzysta z metody
`bayes.get_features(item)`, żeby uzyskać zbiór cech. Należy zwiększać
słownik `bayes.feature_count` dla każdej pary cechy i kategorii o
jeden. W słowniku `bayes.class_count` należy zliczać, ile dokumentów
należy do danej kategorii.

NAME: train
PARAMS: NaiveBayes, string, string
RETURN: -
POINTS: 2
"""

def train(bayes, item, cat):
    'ale dzis zimnoooooo....'
    for cecha in bayes.get_features(item):
        tmp = bayes.feature_count.get((cecha, cat), 0)
        bayes.feature_count[(cecha, cat)] = tmp +1
        
    tmp = bayes.class_count.get(cat, 0)
    bayes.class_count[cat] = tmp+1