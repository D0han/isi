# -*- coding: utf-8 -*-

"""
Zadanie 704

Napisz funkcję `all_members(bicluster)`, które zwraca wszystkie
elementy skupienia.

NAME: all_members
PARAMS: Bicluster
RETURN: list
POINTS: 2
"""

import unittest
import bicluster
from Task704 import all_members

class Task704Test(unittest.TestCase):
    """Testy do zadania 704."""

    def test(self):
        """Test na jednej macierzy."""

        leaf_clusters = [
            bicluster.Bicluster([4.0, 3.0, 0.0, 4.0, 13.0, 1.0], cluster_id=0),
            bicluster.Bicluster([3.0, 0.0, 1.0, 0.0, 2.0, 11.0], cluster_id=1),
            bicluster.Bicluster([6.0, 0.0, 0.0, 7.0, 1.0, 0.0], cluster_id=2),
            bicluster.Bicluster([2.0, 2.0, 1.0, 1.0, 4.0, 5.0], cluster_id=3),
            bicluster.Bicluster([2.0, 1.0, 0.0, 1.0, 1.0, 5.0], cluster_id=4)
        ]

        self.assertEqual(
            all_members(leaf_clusters[0]),
            [ [4.0, 3.0, 0.0, 4.0, 13.0, 1.0 ] ])

        fake_vec = [0.0] * 6

        cluster_a = bicluster.Bicluster(
            fake_vec,
            left = leaf_clusters[0],
            right = leaf_clusters[3],
            cluster_id = 5)

        self.assertItemsEqual(
            all_members(cluster_a),
            [ [4.0, 3.0, 0.0, 4.0, 13.0, 1.0 ],
              [2.0, 2.0, 1.0, 1.0, 4.0, 5.0 ] ])

        cluster_b = bicluster.Bicluster(
            fake_vec,
            left = cluster_a,
            right = leaf_clusters[4],
            cluster_id = 6)

        self.assertItemsEqual(
            all_members(cluster_b),
            [ [4.0, 3.0, 0.0, 4.0, 13.0, 1.0 ],
              [2.0, 2.0, 1.0, 1.0, 4.0, 5.0 ],
              [2.0, 1.0, 0.0, 1.0, 1.0, 5.0] ])

        cluster_c = bicluster.Bicluster(
            fake_vec,
            left = leaf_clusters[1],
            right = leaf_clusters[2],
            cluster_id = 7)

        self.assertItemsEqual(
            all_members(cluster_c),
            [ [3.0, 0.0, 1.0, 0.0, 2.0, 11.0],
              [6.0, 0.0, 0.0, 7.0, 1.0, 0.0] ])

        cluster_d = bicluster.Bicluster(
            fake_vec,
            left = cluster_b,
            right = cluster_c,
            cluster_id = 8)

        self.assertItemsEqual(
            all_members(cluster_d),
            [ [3.0, 0.0, 1.0, 0.0, 2.0, 11.0],
              [6.0, 0.0, 0.0, 7.0, 1.0, 0.0],
              [4.0, 3.0, 0.0, 4.0, 13.0, 1.0 ],
              [2.0, 2.0, 1.0, 1.0, 4.0, 5.0 ],
              [2.0, 1.0, 0.0, 1.0, 1.0, 5.0] ])

if __name__ == '__main__':
    unittest.main()
