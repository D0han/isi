#!/usr/bin/python
# encoding: utf-8

"""Implementacja algorytmu k-średnich."""

import sys
import codecs
import optparse

import distances

from Task606 import print_clusters
from Task607 import kcluster

def read_file(fileobj):
    """
    Wczytuje macierz cech, zwraca listę dokumentów, listę wyrazów
    oraz czystą macierz cech bez informacji o nagłówkach
    """

    lines = [line for line in fileobj]
    colnames = lines[0].strip().split('\t')[1:]
    rownames = []
    data = []
    for line in lines[1:]:
        fields = line.strip().split('\t')
        rownames.append(fields[0])
        data.append([float(x) for x in fields[1:]])
    return rownames, colnames, data

if __name__ == '__main__':
    PARSER = optparse.OptionParser()
    PARSER.add_option('-k', action="store", dest="k", type="int", default="4")
    PARSER.add_option('--dist', action="store", dest="d", type="int",
                      default="0")
    (OPTIONS, ARGS) = PARSER.parse_args()

    sys.stdin = codecs.getreader('utf8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)

    LABELS, WORDS, DATA = read_file(sys.stdin)

    # wybiera miare odległości: 0 - współczynnik korelacji, 1 -
    # odległość oparta na kącie między wektorami, 2 - odległość
    # euklidesowa

    if OPTIONS.d == 1:
        DISTANCE = distances.cosine
    elif OPTIONS.d == 2:
        DISTANCE = distances.euclidean
    else:
        DISTANCE = distances.pearson

    CLUSTERS = kcluster(DATA, distance=DISTANCE, k=OPTIONS.k)
    print_clusters(CLUSTERS, LABELS, sys.stdout)
