# -*- coding: utf-8 -*-

"""
Zadanie C04

Napisz funkcję `most_frequent_class(rows)`, która zwraca najczęstszą
klasę wśród wektorów `rows`. Jeśli więcej niż jedna klasa ma
największą liczbę wystąpień, można zwrócić dowolną z tych klas.
Zakładamy, że nazwa klasy jest zawsze ostatnim elementem wektora.
Można skorzystać z rozwiązania zadania C03.

NAME: most_frequent_class
PARAMS: list
RETURN: string
POINTS: 2
"""

import unittest
from TaskC04 import most_frequent_class

class TaskC04Test(unittest.TestCase):
    """Testy do zadania TaskC04."""

    def test(self):
        """Prosty test."""

        rows = [
            [4, 180, 'bear'],
            [2, 200, 'bigfoot'],
            [2, 190, 'bigfoot'],
            [2, 170, 'human'],
            [2, 160, 'bear'],
            [0, 150, 'crocodile']]

        self.assertIn(most_frequent_class(rows), ['bigfoot', 'bear'])

        rows.append([4, 195, 'bigfoot'])

        self.assertEqual(most_frequent_class(rows), 'bigfoot')

if __name__ == '__main__':
    unittest.main()
