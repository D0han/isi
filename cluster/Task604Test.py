# -*- coding: utf-8 -*-

"""
Zadanie 604

Napisz funkcję `separate_data(centroids, data, distance)`, która
zwraca listę K grup identyfikatorów dokumentów (K to liczba
centroidów). Identyfikator dokumentu, to indeks jego wektora w
macierzy `data`. `Centroids` jest listą K centroidów, data jest
macierzą cech, `distance` jest stosowaną funkcją odległości. Funkcja
ma przydzielać dokument do i-tej grupy, jeżeli odległość wektora tego
dokumentu od i-tego centroidu jest mniejsza niż dla pozostałych
centroidów.

NAME: separate_data
PARAMS: list, list, function
RETURN: list
POINTS: 3
"""

import distances

import unittest
from Task604 import separate_data

class Task604Test(unittest.TestCase):
    """Testy do zadania 604."""

    def test(self):
        """Prosty test na sztucznej macierzy."""

        matrix = [
            [4.0, 3.0, 0.0, 4.0, 13.0],
            [3.0, 0.0, 1.0, 0.0, 2.0],
            [6.0, 0.0, 0.0, 7.0, 1.0],
            [2.0, 2.0, 1.0, 1.0, 4.0],
            [2.0, 1.0, 0.0, 1.0, 1.0]
        ]

        centroids = [
            [3.2644328743503568, 2.7934728644286571, 0.01658522304574328,
             6.2708699919820141, 12.005606251963332],
            [4.5646558840759699, 2.4540935924358021, 0.8676228638991127,
             6.270117813042738, 3.4442032277680501],
            [5.6932630557785586, 0.89876452517495475, 0.93903459060734684,
             6.9499840123625969, 1.1570764030657847]
        ]

        clusters1 = [[0], [1, 3, 4], [2]]
        clusters2 = [[0, 3], [1, 4], [2]]

        self.assertEqual(separate_data(centroids, matrix, distances.euclidean),
                         clusters1)
        self.assertEqual(separate_data(centroids, matrix, distances.cosine),
                         clusters2)


if __name__ == '__main__':
    unittest.main()
