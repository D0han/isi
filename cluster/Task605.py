# -*- coding: utf-8 -*-

"""
Zadanie 605

Napisz funkcję `average_centroids(centroids, clusters, data)`, która
zwraca listę K nowych centroidów na podstawie grup w `clusters`. Każda
grupa składa się z identyfikatorów wektorów (wierszy) z macierzy `data`.
Nowy i-ty centroid obliczamy wektorowo jako sumę wektorów i-tej grupy
(porbranych z macierzy data) podzieloną przez liczbę dokumentów w
i-tej grupie. Uzyskamy w ten sposób uśrednione centroidy. Jeśli i-ta
grupy jest pusta, należy jako nowy i-ty centroid przyjąć poprzedni
i-ty centroid z centroids.

NAME: average_centroids
PARAMS: list, list, function
RETURN: list
POINTS: 3
"""

def average_centroids(centroids, clusters, data):
    "asd"
    

    wyn = []
    for clust in range(len(clusters)):
        if not clusters[clust]:
            wyn.append(centroids[clust])
        else:
            tmp = [0]*len(data[0])
            for wiersz in clusters[clust]:
                for elem in range(len(data[wiersz])):
                    tmp[elem] += data[wiersz][elem]
            wyn.append([x/len(clusters[clust]) for x in tmp])

    return wyn

