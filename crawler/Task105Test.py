#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 105

Napisz metodę `SimpleCrawler.get_links(self, page, soup)`, która za pomocą
modułu `BeautifulSoup` wyciąga linki z podanej strony i zwraca zbiór
linków. Obiekt soup zawiera już przeparsowaną stronę HTML, można
bezpośrednio z niego korzystać. Parametr `page` zawiera pełny adres
przeparsowanej strony, za jego pomocą oraz wykorzystując moduł
urlparse można automatycznie uzupełnić linki względne. W adresach
należy obciąć wszystko na prawo od znaku # ze znakiem włącznie.
Adresy, które po sklejeniu nie zawierają przedrostek `http://`, oraz
adresy, które zawierają znak apostrofu należy odrzucić.

NAME: get_links
PARAMS: self, string, soup
RETURN: set
POINTS: 3
"""

import unittest
from SimpleCrawler import SimpleCrawler
from BeautifulSoup import BeautifulSoup
import urllib

class Task105Test(unittest.TestCase):
    """Testy do zadania 105."""

    def test(self):
        """Testy na stronie MJD."""
        crawler = SimpleCrawler("test105.db")

        page = u'http://amu.edu.pl/~junczys'
        soup = BeautifulSoup(urllib.urlopen(page))

        prefix = page + u'/index.php?title='

        reflinks = set(
            [u'http://poleng.pl/poleng/en/node/730',
             u'http://www.mediawiki.org/',
             u'http://www.staff.amu.edu.pl/~junczys/index.php?title=Main_Page',
             page,
             u'http://amu.edu.pl/~junczys',
            ] +
            [
             prefix + section for section in \
                 [u'Main_Page',
                  u'Laboratorium_programowania',
                  u'Programowanie_2',
                  u'Talk:For_students/Dla_student%C3%B3w:' \
                      + u'_Nowy_dzia%C5%82_o_prowadzonych_zaj%C4%99ciach',
                  u'For_students/Dla_student%C3%B3w:'\
                      + u'_Nowy_dzia%C5%82_o_prowadzonych_zaj%C4%99ciach',
                  u'Publications',
                  u'Contact',
                  u'About_me',
                  u'Polish-French_SMT:_Beta-Version_available',
                  u'Research',
                  u'Special:UserLogin&returnto=Main_Page',
                  u'SQL',
                  u'Teaching',
                  u'Talk:Polish-French_SMT:_Beta-Version_available',
                  u'User:Junczys'
                  ]
             ])

        tstlinks = crawler.get_links(page, soup)

        self.failUnlessEqual(tstlinks, reflinks)

if __name__ == '__main__':
    unittest.main()
