﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 204

Napisz funkcję `calc_tf_idf(simple_index, term, document)`, 
która na podstawie zawartości indeksu oblicza wartość TF-IDF 
dla danego wyrazu i dokumentu. Nalezy oczywiście wykorzystać 
funkcje `calc_tf` oraz `calc_idf` (nie ma więc sensu rozwiązywać 
to zadanie bez wcześniejszego rozwiązania zadań 202 i 203).

NAME: calc_tf_idf
PARAMS: SimpleIndex, string, string
RETURN: float
POINTS: 1
"""

from Task202 import calc_tf
from Task203 import calc_idf

def calc_tf_idf(simple_index, term, document):
    "lubie placki"
    return calc_tf(simple_index, term, document) * calc_idf(simple_index, term)