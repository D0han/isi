# -*- coding: utf-8 -*-

"""
Zadanie 603

Napisz funkcję `random_centroids(kparam, data)`, która zwraca listę
`kparam` losowo zainicjalizowanych centroidów. Centroid to wektor o
takiej samej długości jak wiersze w macierzy `data`. Wartość na i-tym
miejscu w centroidzie powinna być losową wartością leżącą między
minimum a maksimum i-tej kolumny macierzy `data`. Uwaga: funkcja ma
faktycznie zwracać losowe centroidy, jeśli nie ma będzie zwracać, to
punkty nie będą przyznane nawet jeśli funkcja przejdzie testy.

NAME: random_centroids
PARAMS: int, list
RETURN: list
POINTS: 2
"""

import unittest
from Task603 import random_centroids

class Task603Test(unittest.TestCase):
    """Testy do zadania 603."""

    def test(self):
        """Prosty test na sztucznej macierzy."""

        matrix = [
            [4.0, 3.0, 0.0, 4.0, 13.0],
            [3.0, 0.0, 1.0, 0.0, 2.0],
            [6.0, 0.0, 0.0, 7.0, 1.0],
            [2.0, 2.0, 1.0, 1.0, 4.0],
            [2.0, 1.0, 0.0, 1.0, 1.0]
        ]

        mranges = [(6.0, 2.0), (3.0, 0.0), (1.0, 0.0), (7.0, 0.0), (13, 1.0)]

        centroids1 = random_centroids(3, matrix)
        centroids2 = random_centroids(3, matrix)

        self.assertNotEqual(centroids1, centroids2)

        for centroids in [centroids1, centroids2]:
            self.assertEqual(len(centroids), 3)
            for centroid in centroids:
                for value, mrange in zip(centroid, mranges):
                    self.failUnless(mrange[0] >= value >= mrange[1])

if __name__ == '__main__':
    unittest.main()
