﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 202

Napisz funkcję `calc_tf(simple_index, term, document)`, która na
podstawie zawartości indeksu oblicza liczbę wystąpień wyrazu `term`
w dokumencie o nazwie `document`.

NAME: calc_tf
PARAMS: SimpleIndex, string, string
RETURN: int
POINTS: 2
"""

def calc_tf(simple_index, term, document):
    "costam robi"
    indeks = simple_index.documents[document]
    simple_index.inverted_index.setdefault(term, [])
    return simple_index.inverted_index[term].count(indeks)
    