# -*- coding: utf-8 -*-

"""Rozwiązanie zadania 101."""

def even_elements(inlist):
    """Zwraca elementy listy o indeksach parzystych."""
    return [ e for i, e in enumerate(inlist) if i % 2 == 0 ]

if __name__ == '__main__':
    print even_elements([1, 2, 3, 4])
