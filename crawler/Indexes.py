# -*- coding: utf-8 -*-

"""Materiały do zadania na I ćwiczenia."""

class DummyIndex:
    """Indeks, który nic nie robi"""
    
    def process(self, url, content):
        """"Funkcja, która nic nie robi"""
        pass
    
class Dumper:
    """Indeks, który wyrzuca treść stron do pliku"""
    def __init__(self, filename):
        """Konstruktor"""
        self.dump = open(filename, "w")
        self.count = 0
    
    def __del__(self):
        """Destruktor"""
        self.dump.close()
        
    def process(self, url, content):
        """Zapisz do pliku"""
        self.dump.write("Record: %i\n" % self.count) 
        self.dump.write("%s\n" % url) 
        self.dump.write(content)
        self.dump.write("\n\n")
        self.count += 1