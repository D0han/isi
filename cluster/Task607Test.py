# -*- coding: utf-8 -*-

"""
Zadanie 607

Napisz funkcję `kcluster(data, distance=distances.cosine, k=3, iterations=100)`,
która jest implementacją algorytmu k-średnich. Wykorzystaj wcześniej napisane
funkcje random_centroids, separate_data oraz average_centroids. Domślnie używana
odległość to odległóść Pearsona, domyślna liczba klas to 3, domyślna liczba iteracji
to 100. Funkcja powinna zakończyć działanie wcześniej, jeżeli centroidy w aktualnej
iteracji nie różnią się od centroidów ostatniej iteracji.

NAME: kcluster
PARAMS: list, function, int, int
RETURN: list
POINTS: 3
"""

import unittest

import distances
from Task607 import kcluster

class Task607Test(unittest.TestCase):
    """Testy do zadania 607."""

    def test(self):
        """Prosty test na sztucznej macierzy."""

        matrix = [
            [7.0, 3.0, 0.0, 4.0, 0.0],
            [3.0, 0.0, 1.0, 0.0, 2.0],
            [8.0, 0.0, 0.0, 7.0, 1.0],
            [2.0, 0.0, 1.0, 1.0, 4.0],
            [2.0, 1.0, 0.0, 1.0, 1.0]
        ]

        for length in range(1, 5):
            clusters = kcluster(matrix, distance=distances.cosine, k=length)

            # sprawdza poprawną liczbę skupień
            self.assertEqual(length, len(clusters))

            # sprawdza czy skupienia są poprawnie rozdzielone
            for i in range(0, length):
                for clusterId in clusters[i]:
                    for j in range(0, length):
                        if i != j:
                            self.failUnless(clusterId not in clusters[j])


if __name__ == '__main__':
    unittest.main()
