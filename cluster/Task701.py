# -*- coding: utf-8 -*-

"""
Zadanie 701

Napisz funkcję `initialize_clusters(data)`, która zwraca listę grup
(singletonów), czyli obiektów `bicluster`. Każdemu wierszu z macierzy
cech `data` odpowiada jedna grupa. Identyfikator grupy jest numerem
kolejnego wiersza. Pozostałe pola obiektu bicluster przyjmują wartości
domyślne.

NAME: initialize_clusters
PARAMS: list
RETURN: list
POINTS: 1
"""

import bicluster

def initialize_clusters(data):
    "ale faza"
    
    wynik = [bicluster.Bicluster(wiersz, cluster_id=numer) for
                wiersz, numer in zip(data, range(len(data)))]
    return wynik
    