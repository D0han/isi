# -*- coding: utf-8 -*-

"""
Zadanie 602

Napisz funkcję `pearson(vec1, vec2)`, która oblicza 1 - r (współczynnik
korelacji Pearsona) dla dwóch podanych wektorów.

NAME: pearson
PARAMS: list, list
RETURN: float
POINTS: 2
"""

import math

def pearson(vec1, vec2):
    "asd"

    tmp1 = sum([x*y for x, y in zip(vec1, vec2)])
    tmp2 = ((sum([x for x in vec1])*sum([x for x in vec2]))/float(len(vec1)))
    
    licznik = tmp1 - tmp2
    
    tmp1 = sum([x**2 for x in vec1])
    tmp2 = sum(vec1)**2/float(len(vec1))
    
    tmp3 = sum([x**2 for x in vec2])
    tmp4 = sum(vec2)**2/float(len(vec2))
    
    mian = math.sqrt((tmp1-tmp2)*(tmp3-tmp4))
    
    return 1-(licznik/mian)
    