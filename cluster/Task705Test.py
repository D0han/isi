# -*- coding: utf-8 -*-

"""
Zadanie 705

Napisz funkcję `single_linkage_cluster_distance(first_cluster,
second_cluster, distance)`, która wylicza odległość między skupieniami
na zasadzie "single_linkage", tj. jako minimalną odległość spośród
odległości między wszystkimi możliwymi parami obiektów obu skupień.
Argument distance to referencja do dowolnej funkcji wyznaczającej
odległość między obiektami.

NAME: single_linkage_cluster_distance
PARAMS: Bicluster, Bicluster, distance
RETURN: float
POINTS: 2
"""

import unittest
import bicluster
import distances
from Task705 import single_linkage_cluster_distance

class Task705Test(unittest.TestCase):
    """Testy do zadania 705."""

    def test(self):
        """Test na jednej macierzy."""

        leaf_clusters = [
            bicluster.Bicluster([4.0, 3.0, 0.0, 4.0, 13.0, 1.0], cluster_id=0),
            bicluster.Bicluster([3.0, 0.0, 1.0, 0.0, 2.0, 11.0], cluster_id=1),
            bicluster.Bicluster([6.0, 0.0, 0.0, 7.0, 1.0, 0.0], cluster_id=2),
            bicluster.Bicluster([2.0, 2.0, 1.0, 1.0, 4.0, 5.0], cluster_id=3),
            bicluster.Bicluster([2.0, 1.0, 0.0, 1.0, 1.0, 5.0], cluster_id=4)
        ]

        fake_vec = [0.0] * 6

        cluster_a = bicluster.Bicluster(
            fake_vec,
            left = leaf_clusters[0],
            right = leaf_clusters[3],
            cluster_id = 5)

        cluster_b = bicluster.Bicluster(
            fake_vec,
            left = cluster_a,
            right = leaf_clusters[4],
            cluster_id = 6)

        cluster_c = bicluster.Bicluster(
            fake_vec,
            left = leaf_clusters[1],
            right = leaf_clusters[2],
            cluster_id = 7)

        self.assertAlmostEqual(
            single_linkage_cluster_distance(
                leaf_clusters[0],
                leaf_clusters[1],
                distances.euclidean),
            distances.euclidean(
                leaf_clusters[0].vec,
                leaf_clusters[1].vec))

        self.assertAlmostEqual(
            single_linkage_cluster_distance(
                cluster_c,
                cluster_b,
                distances.euclidean),
            distances.euclidean(
                leaf_clusters[1].vec,
                leaf_clusters[4].vec))

if __name__ == '__main__':
    unittest.main()
