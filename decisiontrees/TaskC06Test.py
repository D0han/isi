# -*- coding: utf-8 -*-

"""
Zadanie C06

Napisz funkcję `best_criteria(rows, scoref)`, która wylicza najlepsze
kryterium dla drzewa decyzyjnego dla wektorów `rows` według miary
niejednorodności `scoref`, tj. funkcja powinna zwrócić piątkę (column,
value, decision_function, rows_true, rows_false), gdzie `column` to
numer kolumny, której wartość będzie porównywana z `value`,
`decision_function` to funkcja decyzyjna (dla `column` i `value`),
`rows_true` - wektory, dla których funkcja decyzyjna zwraca `True`,
`rows_false` - wektory, dla których zwracane jest `False`. Najlepsze
kryterium to takie, które najbardziej zmniejsza sumaryczną miarę dla
`rows_true` i `rows_false` (miary dla obu tych zbiorów powinny być
ważone względem liczności `rows_true` i `rows_false`). Jeśli tak
zmierzona miara nie będzie mniejsza niż miara dla oryginalnego zbioru
`rows`, należy zwrócić wartość specjalną `None`. Należy skorzystać z
rozwiązań zadań C01 i C02.

NAME: best_criteria
PARAMS: list, function
RETURN: tuple
POINTS: 4
"""

import unittest
from TaskC06 import best_criteria

def number_of_classes(rows):
    """
    Do testów jako miary użyjemy prostej funkcji, która zlicza liczbę
    różnych klas w `rows` (przypominam, że klasa to ostatni element
    wektora). W praktyce nie używa się takiej funkcji.
    """
    return len(set([row[-1] for row in rows]))

class TaskC06Test(unittest.TestCase):
    """Testy do zadania TaskC06."""

    def test_simple(self):
        """Prosty test."""

        best_one = best_criteria(
            [['foo', 'bar', 'yes'],
             ['foo', 'baz', 'no']],
            number_of_classes)

        self.assertEqual(len(best_one), 5)
        self.assertEqual(best_one[0], 1)
        self.assertIn(best_one[1], ['bar', 'baz'])
        if best_one[1] == 'bar':
            self.assertTrue(best_one[2](['fix', 'bar', 'yes']))
            self.assertFalse(best_one[2](['fix', 'baq', 'yes']))
            self.assertEqual(best_one[3], [['foo', 'bar', 'yes']])
            self.assertEqual(best_one[4], [['foo', 'baz', 'no']])
        else:
            self.assertTrue(best_one[2](['fix', 'baz', 'yes']))
            self.assertFalse(best_one[2](['fix', 'baq', 'yes']))
            self.assertEqual(best_one[4], [['foo', 'bar', 'yes']])
            self.assertEqual(best_one[3], [['foo', 'baz', 'no']])

    def test_more_classes(self):
        """Nieco bardziej skomplikowany test."""

        best_one = best_criteria(
            [['black', 'aaa', 'foo', 10, 'maybe'],
             ['red', 'bbb', 'foo', 10, 'yes'],
             ['blue', 'ccc', 'foo', 10, 'no'],
             ['red', 'ddd', 'bar', 10, 'yes']],
            number_of_classes)

        self.assertEqual(best_one[0], 0)
        self.assertEqual(best_one[1], 'red')
        self.assertTrue(best_one[2](['red', 'bbb', 'baz', 12, 'maybe']))
        self.assertFalse(best_one[2](['blue', 'bbb', 'baz', 12, 'maybe']))
        self.assertEqual(best_one[3],
                         [['red', 'bbb', 'foo', 10, 'yes'],
                          ['red', 'ddd', 'bar', 10, 'yes']])
        self.assertEqual(best_one[4],
                         [['black', 'aaa', 'foo', 10, 'maybe'],
                          ['blue', 'ccc', 'foo', 10, 'no']])


    def test_numbers(self):
        """Test warunku liczbowego (porównania)."""

        best_one = best_criteria(
            [['foo', 2, 'xxx', 'no'],
             ['foo', 4, 'xxx', 'yes'],
             ['bar', 3, 'xxx', 'yes'],
             ['baz', 1, 'xxx', 'no']],
            number_of_classes)

        self.assertEqual(best_one[0], 1)
        self.assertEqual(best_one[1], 3)
        self.assertTrue(best_one[2](['foo', 100, 'xxx', 'no']))
        self.assertTrue(best_one[2](['foo', 3, 'xxx', 'no']))
        self.assertFalse(best_one[2](['foo', 0, 'xxx', 'no']))
        self.assertEqual(best_one[3],
                        [['foo', 4, 'xxx', 'yes'],
                         ['bar', 3, 'xxx', 'yes']])
        self.assertEqual(best_one[4],
                        [['foo', 2, 'xxx', 'no'],
                         ['baz', 1, 'xxx', 'no']])


    def test_trivial_none(self):
        """Test trywialnego przypadku, gdy powinno być zwrócone None."""

        self.assertIsNone(best_criteria(
            [['foo', 'no'],
             ['foo', 'no']],
            number_of_classes))

    def test_complicated_none(self):
        """
        Test bardziej skomplikowanego przypadku, gdy powinno być
        zwrócone None.
        """

        self.assertIsNone(best_criteria(
                [['foo', 3, 'yes'],
                 ['bar', 3, 'yes'],
                 ['foo', 3, 'no'],
                 ['bar', 3, 'no'],
                 ['foo', 3, 'maybe'],
                 ['bar', 3, 'maybe']],
                number_of_classes))

if __name__ == '__main__':
    unittest.main()
