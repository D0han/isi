﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 206

Napisz funkcję `cosine_score(simple_index, query, document)`,
 która dla zapytania (lista wyrazów) i zadanego dokumentu 
 oblicza kosinus konta między wektorami zapytania i dokumentu.
 Wektor dokumentu powinien się składac z wartości TF-IDF 
 (`calc_tf_idf`) dla każdego wyrazu występującego w zapytaniu 
 oraz w całym indeksie. Wektor zapytania powinien składać 
 się samych zer dla wyrazów niewystępujących w zapytaniu i 
 z wartośći IDF (`calc_idf`) przemnożonych przez liczbę 
 występowanie w zapytaniu dla wyrazów z zapytania. Wektory 
 musza mieć oczywiście równe długości; wartości na tych samych 
 pozycjach muszą odpowiadać tym samym wyrazom. Uwaga: w wektorach 
 należy uwzględnić zarówno termy z kolekcji dokumentów, jak 
 i zapytania (proszę użyć union na odpowiednich zbiorach). 
 Należy wykorzystać rozwiązanie zadanie 204 do wyliczenia TF-IDF.

NAME: cosine_score
PARAMS: SimpleIndex, list, string
RETURN: float
POINTS: 4
"""

#import operator
from math import sqrt
from Task203 import calc_idf
from Task204 import calc_tf_idf

def cosine_score(simple_index, query, document):
    "bo tak"
    doc_nr = simple_index.documents[document]
    words_set = set(query) # zbior wyrazow w dokumencie + zapytaniu
    for word, doc_list in simple_index.inverted_index.iteritems():
        if doc_nr in doc_list:
            words_set.add(word)
    query_vect = []
    doc_vect   = []
    # wektory
    for word in words_set:
        query_vect.append( calc_idf(simple_index, word) * query.count(word))
        doc_vect.append(   calc_tf_idf(simple_index, word, document))
#    dot_product = sum(map(operator.mul, query_vect, doc_vect))
    dot_product = sum(p*q for p, q in zip(query_vect, doc_vect))
    query_euc_len = sqrt(sum([x*x for x in query_vect]))
    doc_euc_len   = sqrt(sum([x*x for x in doc_vect]))
    return dot_product/(query_euc_len * doc_euc_len)
