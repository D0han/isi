# -*- coding: utf-8 -*-

"""
Zadanie 701

Napisz funkcję `initialize_clusters(data)`, która zwraca listę grup
(singletonów), czyli obiektów `bicluster`. Każdemu wierszu z macierzy
cech `data` odpowiada jedna grupa. Identyfikator grupy jest numerem
kolejnego wiersza. Pozostałe pola obiektu bicluster przyjmują wartości
domyślne.

NAME: initialize_clusters
PARAMS: list
RETURN: list
POINTS: 1
"""

import unittest
import bicluster
from Task701 import initialize_clusters

class Task701Test(unittest.TestCase):
    """Testy do zadania 701."""

    def test(self):
        """Test na jednej macierzy."""

        matrix = [
            [4.0, 3.0, 0.0, 4.0, 13.0, 1.0],
            [3.0, 0.0, 1.0, 0.0, 2.0, 11.0],
            [6.0, 0.0, 0.0, 7.0, 1.0, 0.0],
            [2.0, 2.0, 1.0, 1.0, 4.0, 5.0],
            [2.0, 1.0, 0.0, 1.0, 1.0, 5.0]
        ]

        expected_clusters = [
            bicluster.Bicluster([4.0, 3.0, 0.0, 4.0, 13.0, 1.0], cluster_id=0),
            bicluster.Bicluster([3.0, 0.0, 1.0, 0.0, 2.0, 11.0], cluster_id=1),
            bicluster.Bicluster([6.0, 0.0, 0.0, 7.0, 1.0, 0.0], cluster_id=2),
            bicluster.Bicluster([2.0, 2.0, 1.0, 1.0, 4.0, 5.0], cluster_id=3),
            bicluster.Bicluster([2.0, 1.0, 0.0, 1.0, 1.0, 5.0], cluster_id=4)
        ]

        clusters = initialize_clusters(matrix)

        self.assertEqual(clusters, expected_clusters)

if __name__ == '__main__':
    unittest.main()
