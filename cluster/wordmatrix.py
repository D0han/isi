#!/usr/bin/python
# encoding: utf-8

"""Program wypisujący macierz cech."""

import sys
import codecs
import optparse
import glob
import re

def get_word_counts(filelist):
    """Zwraca listy frekwencyjne."""
    return dict([(filepath, get_freq_list(filepath)) for
                 filepath in filelist])

def get_freq_list(filepath):
    """Zwraca listę frekwencyjną dla pojedynczego pliku."""
    freq_list = dict()
    for term in tokenize_file(filepath):
        freq_list.setdefault(term, 0)
        freq_list[term] += 1
    return freq_list

def tokenize_file(filepath):
    """Zwraca stokenizowany plik."""
    file_obj = codecs.open(filepath, "r", "utf-8")
    return sum([tokenize_line(line.lower())
                for line in file_obj.readlines()], [])

def tokenize_line(line):
    """Zwraca stokenizowany wiersz."""
    word_regex = re.compile(ur"[a-ząćęłńóśźż]+", re.UNICODE)
    return word_regex.findall(line)

def get_word_list(wordcounts, minf=0.02, maxf=0.30):
    """Zwraca listę znaczących wyrazów."""
    number_of_files = len(wordcounts)
    document_freq = dict()
    for freq_list in wordcounts.values():
        for word in freq_list.keys():
            document_freq.setdefault(word, 0)
            document_freq[word] += 1

    return [ word for word in document_freq
             if document_freq[word]/float(number_of_files) > minf
                and document_freq[word]/float(number_of_files) < maxf ]

def write_matrix(wordcounts, wordlist):
    """Wypisuje macierz na standardowe wyjście."""
    sorted_wordlist = sorted(wordlist)
    print "\t".join(["Name"] + sorted_wordlist)
    for filepath in sorted(wordcounts):
        freq_list = wordcounts[filepath]
        print "\t".join(
            [filepath]
            + [str(freq_list.get(word, 0))
               for word in sorted_wordlist])

if __name__ == '__main__':
    PARSER = optparse.OptionParser()
    PARSER.add_option('--dir', action="store",
                      dest="dir", type="string", default=".")
    PARSER.add_option('--min', action="store",
                      dest="min", type="float", default="0.05")
    PARSER.add_option('--max', action="store",
                      dest="max", type="float", default="0.50")
    (OPTIONS, ARGS) = PARSER.parse_args()
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)

    FILELIST = glob.glob(OPTIONS.dir + "/*")

    WORDCOUNTS = get_word_counts(FILELIST)
    WORDLIST = get_word_list(WORDCOUNTS, minf=OPTIONS.min, maxf=OPTIONS.max)
    write_matrix(WORDCOUNTS, WORDLIST)
