# -*- coding: utf-8 -*-

"""
Zadanie 804

Napisz funkcję `docprob(bayes, item, cat)`, która oblicza uogólnioną
sumę z równania (5) - łącznie z prawdopodobieństwem kategorii.
Podobnie jak w zadaniu 801 wykorzystujemy funkcję
`self.get_features(item)` w celu uzyskania zbioru cech dokumentu
`item`. Skorzystać z rozwiązań zadań 802 i 803.

NAME: docprob
PARAMS: NaiveBayes, item, cat
RETURN: float
POINTS: 2
"""

from Task802 import featprob
from Task803 import catprob

def docprob(bayes, item, cat):
    'wodyyyyyyyyyyyyy'
    tmp = sum([featprob(bayes, feature, cat) for \
        feature in bayes.get_features(item)]) + \
        catprob(bayes, cat)
    return tmp