# encoding: utf-8

"""Grupowanie hierarchiczne."""

import sys
import codecs
import optparse

import distances
from bicluster import average_vector_cluster_distance
from Task705 import single_linkage_cluster_distance
from Task706 import hcluster

def read_file(fileobj):
    """Wczytuje plik wygenerowany przez wordmatrix.py."""
    lines = [line for line in fileobj]
    colnames = lines[0].strip().split('\t')[1:]
    rownames = []
    data = []
    for line in lines[1:]:
        fields = line.strip().split('\t')
        rownames.append(fields[0])
        data.append([float(x) for x in fields[1:]])
    return rownames, colnames, data

def print_clusters(cluster, fileobj, labels=None, branches=list([False])):
    """Wypisuje ASCII-dendrogram."""
    for i in branches[:-1]:
        if i:
            fileobj.write('| ')
        else:
            fileobj.write('  ')

    if branches[-1]:
        fileobj.write("|-")
    else:
        fileobj.write("`-")

    if cluster.cluster_id < 0:
        fileobj.write(".\n")
    else:
        if labels == None:
            fileobj.write(str(cluster.cluster_id) + "\n")
        else:
            fileobj.write(labels[cluster.cluster_id] + "\n")

    if cluster.left:
        print_clusters(cluster.left, fileobj,
                       labels=labels, branches=branches + [True])
    if cluster.right:
        print_clusters(cluster.right, fileobj,
                       labels=labels, branches=branches + [False])

if __name__ == '__main__':
    PARSER = optparse.OptionParser()
    PARSER.add_option('--dist', action="store",
                      dest="d", type="int", default="0")
    PARSER.add_option('--clust-dist', action="store",
                      dest="c", type="int", default="0")
    (OPTIONS, ARGS) = PARSER.parse_args()

    sys.stdin = codecs.getreader('utf8')(sys.stdin)
    sys.stdout = codecs.getwriter('utf8')(sys.stdout)

    LABELS, WORDS, DATA = read_file(sys.stdin)

    # wybiera miare odległości: 0 - współczynnik korelacji, 1 -
    # odległość oparta na kącie między wektorami, 2 - odległość
    # euklidesowa

    if OPTIONS.d == 1:
        DISTANCE = distances.cosine
    elif OPTIONS.d == 2:
        DISTANCE = distances.euclidean
    else:
        DISTANCE = distances.pearson

    # wybiera miare odległości: 0 - współczynnik korelacji, 1 -
    # odległość oparta na kącie między wektorami, 2 - odległość
    # euklidesowa

    if OPTIONS.c == 1:
        CLUSTER_DISTANCE = average_vector_cluster_distance
    else:
        CLUSTER_DISTANCE = single_linkage_cluster_distance
    
    CLUSTERS = hcluster(DATA, distance=DISTANCE,
                        cluster_distance=CLUSTER_DISTANCE)
    print_clusters(CLUSTERS, sys.stdout, LABELS)
