# -*- coding: utf-8 -*-

"""
Zadanie 601

Napisz funkcję `euclidean(vec1, vec2)`, obliczającą odległość
euklidesową dla dwóch podanych wektorów.

NAME: euclidean
PARAMS: list, list
RETURN: float
POINTS: 1
"""

import math

def euclidean(vec1, vec2):
    """zadanie 601."""
    
    wyn = 0
    for xyz in range(len(vec1)):
        wyn += (vec1[xyz]-vec2[xyz])**2
    return math.sqrt(wyn)
