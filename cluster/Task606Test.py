# -*- coding: utf-8 -*-

"""
Zadanie 606

Napisz funkcję `print_clusters(clusters, labels, fileobj)`, która wypisuje
do obiektu plikowego `file` informacje o postaci przedstawionej wyżej.
Parametr `clusters` to jak poprzednio lista grup (tzn. list)
identyfikatorów dokumentów. Parametr `labels` zawiera nazwy plików, w
takiej kolejności, że indeks nazwy jest identyfikatorem z parametru
`clusters`. Separatorem jest znak tabulacji.

NAME: print_clusters
PARAMS: list, list, list
RETURN: -
POINTS: 2
"""

import unittest
from Task606 import print_clusters

class Task606Test(unittest.TestCase):
    """Testy do zadania 606."""

    def test(self):
        """Prosty test na sztucznych danych."""

        labels = ["a", "b", "c", "d", "e"]
        clusters = [[0], [1, 3, 4], [2]]

        file1 = open("Task606Test.out.txt", 'w')
        print_clusters(clusters, labels, file1)
        file1.close()

        test1str = open('Task606Test.out.txt').read()
        test2str = open('Task606Test.dat.txt').read()

        self.assertEqual(test1str, test2str)

if __name__ == '__main__':
    unittest.main()
