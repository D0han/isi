﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 201

Napisz funkcję `add_document(simple_index, document, terms)`, która
tworzy odwrócony indeks. Funkcja powinna dodawać do słownika
`simple_index.documents` kolejne nazwy dokumentów jako klucze z
przyporządkowanymi identyfikatorami (pierwszy dodany dokument ma mieć
identyfikator 0, drugi - 1 itd). Z kolei lista `terms` zawiera listę
wyrazów (termów) z dokumentu. Wyrazy mogą się powtarzać. Funkcja ma
także tworzyć indeks `simple_index.inverted_index`, czyli słownik,
który wyrazom przyporządkowuje listę identyfikatorów (czasami
wielokrotnie ten sam identyfikator, jeśli wyraz występuje więcej niż
raz dokumencie).

NAME: add_document
PARAMS: SimpleIndex, string, list
RETURN: None
POINTS: 2
"""


def add_document(simple_index, document, terms):
    "cuda"
    simple_index.documents[document] = len(simple_index.documents)
    for term in terms:
        simple_index.inverted_index.setdefault(term, [])
        temp = simple_index.inverted_index[term]
        temp.append(simple_index.documents[document])
    