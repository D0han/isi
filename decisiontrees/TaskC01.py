# -*- coding: utf-8 -*-

"""
Zadanie C01

Napisz funkcję `create_decision_function(column, value)`, która zwraca
funkcję decyzyjną dla danej wartości `value`. Funkcja decyzyjna jako
argument dostaje wektor, powinna ona porównać element wektora o
numerze `column` z wartością `value`. Dla wartości liczbowych (zarówno
całkowitych, jak i zmiennoprzecinkowych) funkcja decyzyjna powinna
zwracać `True`, gdy element wektora jest większy lub równy `value`,
dla wartości innych typów - tylko wtedy gdy element wektora jest
równy `value`.

NAME: create_decision_function
PARAMS: int, any
RETURN: function
POINTS: 2
"""

def create_decision_function(column, value):
    "ale syf"
    def decision_function(vect):
        "tutaj jest"
        if (value == vect[column]):
            return True
        elif isinstance(value, (int, long, float, complex)):
            return (float(value) <= float(vect[column]))
        else:   
            return False
    return decision_function
    