# -*- coding: utf-8 -*-

"""
Zadanie C05

Napisz funkcję `entropy(rows)`, która oblicza entropię wektorów
(według klas). Zakładamy, że nazwa klasy jest zawsze ostatnim
elementem wektora. Można skorzystać z rozwiązania zadania C03.
Jako podstawy logarytmu należy użyć liczby 2.

NAME: entropy
PARAMS: list
RETURN: float
POINTS: 3
"""

import unittest
from TaskC05 import entropy

class TaskC05Test(unittest.TestCase):
    """Testy do zadania TaskC05."""

    def test_simple(self):
        """Prosty test."""

        self.assertAlmostEqual(
            entropy([[4, 180, 'bear'],
                     [2, 200, 'bigfoot']]),
            1.0,
            4)

        self.assertAlmostEqual(
            entropy([[4, 180, 'bear'],
                     [2, 200, 'bigfoot'],
                     [2, 170, 'human'],
                     [0, 150, 'crocodile']]),
            2.0,
            4)

    def test_complicated(self):
        """Nieco bardziej skomplikowany test."""

        self.assertAlmostEqual(
            entropy([[4, 180, 'bear'],
                     [2, 200, 'bigfoot'],
                     [2, 190, 'bigfoot'],
                     [2, 170, 'human'],
                     [2, 160, 'bear'],
                     [4, 190, 'bear'],
                     [2, 202, 'bigfoot'],
                     [2, 190, 'bigfoot'],
                     [2, 170, 'human'],
                     [2, 162, 'bear'],
                     [0, 152, 'crocodile'],
                     [2, 200, 'bigfoot'],
                     [2, 190, 'bigfoot'],
                     [0, 150, 'crocodile'],
                     [2, 200, 'bigfoot'],
                     [2, 190, 'bigfoot']]),
            1.75,
            4)

    def test_zero_entropy(self):
        """Zerowa entropia (wszystkie wektory z tej samej klasy)."""

        self.assertAlmostEqual(
            entropy([ [2, 200, 'bigfoot' ] ]),
            0.0,
            4)

        self.assertAlmostEqual(
            entropy([ [2, 200, 'bigfoot' ],
                      [2, 180, 'bigfoot'],
                      [4, 190, 'bigfoot'] ]),
            0.0,
            4)

if __name__ == '__main__':
    unittest.main()
