# -*- coding: utf-8 -*-

"""
Zadanie 703

Napisz funkcję `average_vectors(first_idx, second_idx, clusters)`,
która zwraca nowy wektor cech na podstawie wektorów cech grup z listy
`clusters` o indeksach `first_idx` oraz `second_idx`. Nowy wektor składa
się z uśrednionych wartości wektorów (pole `vec`!) tych dwóch grup.

NAME: average_vectors
PARAMS: int, int, list
RETURN: list
POINTS: 2
"""

import unittest
import bicluster
from Task703 import average_vectors

class Task703Test(unittest.TestCase):
    """Testy do zadania 703."""

    def test(self):
        """Test na jednej macierzy."""

        clusters = [
            bicluster.Bicluster([4.0, 3.0, 0.0, 4.0, 13.0, 1.0], cluster_id=0),
            bicluster.Bicluster([3.0, 0.0, 1.0, 0.0, 2.0, 11.0], cluster_id=1),
            bicluster.Bicluster([6.0, 0.0, 0.0, 7.0, 1.0, 0.0], cluster_id=2),
            bicluster.Bicluster([2.0, 2.0, 1.0, 1.0, 4.0, 5.0], cluster_id=3),
            bicluster.Bicluster([2.0, 1.0, 0.0, 1.0, 1.0, 5.0], cluster_id=4)
        ]

        self.assertEqual(
            average_vectors(0, 1, clusters),
            [3.5, 1.5, 0.5, 2.0, 7.5, 6.0])

        self.assertEqual(
            average_vectors(1, 2, clusters),
            [4.5, 0.0, 0.5, 3.5, 1.5, 5.5])

        self.assertEqual(
            average_vectors(2, 3, clusters),
            [4.0, 1.0, 0.5, 4.0, 2.5, 2.5])

        self.assertEqual(
            average_vectors(3, 4, clusters),
            [2.0, 1.5, 0.5, 1.0, 2.5, 5.0])

if __name__ == '__main__':
    unittest.main()
