#!/bin/zsh

. ./vars

cd ..

STUDENT_DIR=`ls -d ${PREFIX}-s*`

echo "USING $STUDENT_DIR"

rm -rf arena
mkdir arena

cp -R $STUDENT_DIR/* arena/
ln -s ../$STUDENT_DIR/.git arena/.git

rm -rf arena/run.sh arena/vars
find arena -regex '.*/Task.*Test\.\(py\|dat\..*\)' -exec rm '{}' ';'

cp "${PREFIX}/run.sh" arena/run.sh
cp "${PREFIX}/vars" arena/vars
cp "${PREFIX}/overrides.txt" arena/overrides.txt

mkdir -p arena/crawler
mkdir -p arena/cluster
mkdir -p arena/classifier
mkdir -p arena/decisiontrees
mkdir -p arena/recommendations

find ${PREFIX} -regex '.*/Task.*Test\.\(py\|dat\..*\)' -print | while read T; do cp $T ${T/${PREFIX}/arena}; done
