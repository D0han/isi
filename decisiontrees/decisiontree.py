# -*- coding: utf-8 -*-

"""Program generujący drzewo decyzyjne."""

import sys

from TaskC01 import create_decision_function
from TaskC03 import divide_by_classes
from TaskC04 import most_frequent_class
from TaskC05 import entropy
from TaskC06 import best_criteria

def read_file(file_obj):
    """Wczytuje plik z danymi."""
    lines = [line for line in file_obj]
    colnames = lines[0].strip().split('\t')[1:]
    rownames = []
    data = []
    for line in lines[1:]:
        fields = line.strip().split('\t')
        rownames.append(fields[0])
        row = []
        for field in fields[1:]:
            try:
                row.append(float(field))
            except:
                row.append(field)
        data.append(row)
    return rownames, colnames, data

def cross_eval(number_of_parts=3, file_obj=sys.stdin, verbose=False):
    """Dokonuje sprawdzenia krzyżowego."""
    labels, feature_labels, data = read_file(file_obj)

    correct = 0
    total   = 0

    if verbose:
        print "Splitting into", number_of_parts, "data sets"

    for i in range(number_of_parts):
        if verbose:
            print "Training on data sets", str(
                [j for j in range(number_of_parts) if j != i])
            print "Testing on data set", i
            print

        test_start = int(len(data)/float(number_of_parts) * i)
        test_end = int(len(data)/float(number_of_parts) * (i+1))

        test = []
        train = []
        for i, row in enumerate(data):
            if(test_start <= i < test_end):
                test.append((i, row))
            else:
                train.append(row)

        dtree = DecisionTree(train, feature_labels)

        if verbose:
            dtree.printtree()

        if verbose:
            print
            print "Classifing with above decision tree:"
        for i, row in test:
            class_guessed = dtree.classify(row[:-1])
            if verbose:
                print "\t", labels[i], "classified as", class_guessed,
            if class_guessed == row[-1]:
                correct += 1
                if verbose:
                    print "- CORRECT"
            else:
                if verbose:
                    print "- WRONG"
            total += 1
        if verbose:
            print

    return float(correct)/total

class DecisionTree:
    """Klasa reprezentująca drzewo decyzyjne."""

    class DecisionNode:
        """Klasa reprezentująca węzeł drzewa decyzyjnego."""
        def __init__(self, col=-1, value=None,
                     final_rows=None, tbranch=None, fbranch=None):
            self.col = col
            self.value = value
            self.final_rows = final_rows
            self.tbranch = tbranch
            self.fbranch = fbranch

    def __init__(self, rows, features=None):
        self.root = self.buildtree(rows, entropy)
        self.features = features

    def classify(self, observation, branch=None):
        """Dokonuje klasyfikacji obiektu o nieznanej klasie."""
        if not branch:
            branch = self.root

        if branch.final_rows != None:
            return most_frequent_class(branch.final_rows)

        else:
            split_function = create_decision_function(
                branch.col, branch.value)

            next_branch = None

            if split_function(observation):
                next_branch = branch.tbranch
            else:
                next_branch = branch.fbranch

        return self.classify(observation, next_branch)


    def buildtree(self, rows, scoref):
        """
        Buduje drzewo decyzyjne na podstawie zadanego zbioru
        trenującego (rows). Jako kryterium używana jest funkcja
        scoref.
        """

        if len(rows) == 0:
            return self.DecisionNode()

        best_one = best_criteria(rows, scoref)

        if best_one is None:
            return self.DecisionNode(final_rows=rows)

        true_branch = self.buildtree(best_one[3], entropy)
        false_branch = self.buildtree(best_one[4], entropy)

        return self.DecisionNode(
            col=best_one[0],
            value=best_one[1],
            tbranch=true_branch,
            fbranch=false_branch)

    def printtree(self, branch=None, file_obj=sys.stdout, branches=[False]):
        """Wypisuje drzewo decyzyjne w postaci czytelnej dla człowieka."""
        if not branch:
            branch = self.root

        for i in branches[:-1]:
            if i:
                file_obj.write('|       ')
            else:
                file_obj.write('        ')

        if branches[-1]:
            file_obj.write("|-|T|-> ")
        else:
            if len(branches) > 1:
                file_obj.write("`-|F|-> ")
            else:
                file_obj.write("`------ ")

        if branch.final_rows == None:
            if isinstance(branch.value, int) \
               or isinstance(branch.value, float):
                file_obj.write("%s >= %s ?\n" %
                           (self.features[branch.col],
                            str(branch.value)))
            else:
                file_obj.write("%s = %s ?\n" %
                           (self.features[branch.col],
                            str(branch.value)))
        else:
            file_obj.write(str(divide_by_classes(branch.final_rows)) + "\n")

        if branch.final_rows == None:
            self.printtree(branch.tbranch,
                           file_obj, branches=branches + [True])
            self.printtree(branch.fbranch,
                           file_obj, branches=branches + [False])

if __name__ == '__main__':
    ACCURACY = cross_eval(number_of_parts=3, verbose=True)
    print "Overall accuracy:" , ACCURACY
