# -*- coding: utf-8 -*-

"""Materiały do zadania na I ćwiczenia."""

import urllib2
import urlparse
import shelve
from BeautifulSoup import BeautifulSoup

# Zaimportuj indeks DummyIndex, który nic nie robi. Później będą tutaj
# inne indeksy. Można już teraz pobawić się indeksem Dumper
import Indexes

############################################################################

class SimpleCrawler:
    """Prosty crawler."""
    def __init__(self, dbname, processor_obj=Indexes.DummyIndex()):
        """Konstruktor"""
        # Konstruktor otwiera połączenie do bazy danych
        # Na razie "ściema", shelve nie wiedzieć
        # czemu wysypuje się na komputerach wydziałowych.
        self.dict = {}
        self.link_db = shelve.Shelf(self.dict, protocol=None, writeback=False)
        #self.link_db = shelve.open(dbname)
        self.processor = processor_obj

    def __del__(self):
        """Destruktor"""
        # Destruktor zamyka połączenie do bazy danych
        self.link_db.close()

    def crawl_one_page(self, url):
        """Przetwarza jedną stronę, zwraca linki znalezione na stronie."""

        # Spróbuj pobrać treść strony, a jeśli się nie uda,
        # to przejdź do następnej strony.
        try:
            urlobject = urllib2.urlopen(url, timeout=5)
            if not self.is_html(urlobject):
                print "\tPage '%s' is not a text or html document." % url
                self.link_db[url] = 2
                return set()
            urlcontent = urlobject.read()
        except KeyboardInterrupt:
            exit()
        except:
            print "\tCould not open '%s'" % url
            self.link_db[url] = 3
            return set()

        # Spróbuj sparsować treść strony, a jeśli się nie uda,
        # to przejdź do następnej strony.
        try:
            soup = BeautifulSoup(urlcontent)
        except KeyboardInterrupt:
            exit()
        except:
            print "\tCould not parse page '%s'" % url
            self.link_db[url] = 4
            return set()

        # Wyciągnij linki ze sparsowanej strony
        links = self.get_links(url, soup)

        # Zaznacz ze strona została odwiedzona
        self.link_db[url] = 1

        # Zaindeksuj stronę i treść
        self.processor.process(url, urlcontent)

        return links

    def crawl_one_level(self):
        """Wykonuje jedną iterację crawlingu."""
        links = set()
        for page, value in self.link_db.iteritems():
            if value == 0:
                links.update(self.crawl_one_page(page))

        return links

    def crawl(self, depth=3):
        """Wykonuje zadaną liczbę iteracji."""
        if depth == 0:
            return
        newlinks = self.crawl_one_level()
        self.inject_urls(newlinks)
        self.crawl(depth-1)

    def inject_urls(self, urls, purge=False):
        """
        Dodaje adresy z listy urls do bazy. Jeśli
        purge ma wartość True, przed dodaniem URL-i
        baza powinna być czyszczona.
        """
        if purge:
            self.link_db.clear()
        
        for url in urls:
            if not url in self.link_db:
                self.link_db[url] = 0

    def is_html(self, urlobject):
        """
        Sprawdza na podstawie nagłówka (nie treści!),
        czy strona jest dokumentem HTML.
        """
        objtype = urlobject.info().gettype()
        return objtype == "text/html"

    def get_links(self, page, soup):
        """
        Zwraca wszystkie linki ze sparsowanej strony.
        """
        links = set()
        for hyperlink in soup.findAll(name='a', href=True):
            link = hyperlink["href"]
            if '#' in link:
                link = link[:link.find('#')]
            if not (link.lower().startswith('http://') or 
                    link.lower().startswith('https://')):
                link = urlparse.urljoin(page, link)

            if not '`' in link:
                links.add(link)
        return links

############################################################################

if __name__ == '__main__':
    MYCRAWLER = SimpleCrawler("crawler.db")
    MYCRAWLER.inject_urls(
        ["http://www.staff.amu.edu.pl/~junczys/index.php?title=Main_Page"],
        True
    )
    MYCRAWLER.crawl(3)
