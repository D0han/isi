# -*- coding: utf-8 -*-

"""
Zadanie 802

Napisz funkcję `featprob(bayes, feature, category)`, która oblicza
prawdopodobieństwo P(F=f|C=c) dla danej cechy (`feature`) i danej
kategorii (`category`). Prawodpodobieństwo warunkowe obliczamy,
dzieląc liczbę współwystępowania cechy `feature` z kategorią
`category` przez liczbę występowania kategorii `category`. Funkcja
zwraca prawdopodobieństwo w postaci logarytmu. Jeżeli dana kategoria
nie istnieje, zwracamy -1e300 (liczba możliwie bliska log(0) = -inf).
Jeśli nie istnieje dana cecha (dla danej kategorii), to zwracamy
logarytm ilorazu liczby 0.001 oraz liczby występowania kategorii
(wygładzanie).

NAME: featprob
PARAMS: NaiveBayes, string, string
RETURN: float
POINTS: 2
"""

from math import log

def featprob(bayes, feature, category):
    'jakmisienicniechce...'
    if category not in bayes.class_count:
        return -1e300
    elif (feature, category) not in bayes.feature_count:
        return log(0.001 / float(bayes.class_count[category]))
    return log(bayes.feature_count[(feature, category)] /\
                    float(bayes.class_count[category]))