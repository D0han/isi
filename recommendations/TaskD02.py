""" D02 """

from math import sqrt

def sim_distance(ratings1, ratings2):
    """ sim_distance """
    set_keys = set(ratings1.keys() + ratings2.keys())
    suma = sum((ratings1.get(key, 0) - ratings2.get(key, 0)) 
        ** 2 for key in set_keys)
    return 1 / (1 + sqrt(suma))