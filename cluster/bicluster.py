# encoding: utf-8

"""Grupowanie hierarchiczne."""

class Bicluster:
    """
    Reprezentacja skupienia. Pole vec to uśredniony wektor (średnia
    uśrednionych wektorów obu podskupień), left - pierwsze
    podskupienie, right - drugie podskupienie, distance - odległość
    między podskupieniami, cluster_id - identyfikator skupienia.
    Dla "zdegenerowanego" skupienia jednoelementowego: left=right=None,
    vec jest po prostu jedynym elementem skupienia.
    """
    def __init__(self, vec, left=None, right=None, cluster_id=None):
        self.left = left
        self.right = right
        self.vec = vec
        self.cluster_id = cluster_id
        self.distance = None

    @staticmethod
    def create_one_element_cluster(vec, cluster_id=None):
        """Pomocnicza metoda zwracająca skupienie jednoelementowe."""
        return Bicluster(vec, cluster_id=cluster_id)

    def set_distance(self, distance):
        """
        Ustawia pomocnicze pole określające odległość między podskupieniami.
        """
        self.distance = distance

    def __eq__(self, other):
        if other:
            return (self.left == other.left
                    and self.right == other.right
                and self.vec == other.vec
                and self.cluster_id == other.cluster_id
                and self.distance == other.distance)
        else:
            return False

def average_vector_cluster_distance(first_cluster, second_cluster, distance):
    """
    Prosta funkcja określająca odległość między skupieniami
    `first_cluster` i `second_cluster` jako odległość między
    uśrednionymi wektorami. Distance to dowolna funkcja odległości
    (działająca na obiektach).
    """
    return distance(first_cluster.vec, second_cluster.vec)
