#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 106

Napisz metodę `SimpleCrawler.crawl_one_level(self)`, która wykonuje
jedną iterację "crawlingu".
Należy wykorzystać funkcję `SimpleCrawler.crawl_one_page`, która ma być
wywoływana dla każdej badanej strony. Należy tylko badać te strony,
które nie zostały wcześniej sprawdzone, czyli których wartość w słowniku
`self.link_db` wynosi 0. Funkcja ma zwrócić zbiór wszystkich nowych linków
danego poziomiu. Do testowania nie trzeba implementować wcześniej funkcji
`get_links` i `is_html`

NAME: crawl_one_level
PARAMS: self
RETURN: set
POINTS: 2
"""

import unittest
from SimpleCrawler import SimpleCrawler
import urllib2

class SimpleCrawlerForTest(SimpleCrawler):
    def __init__(self):
        pass

    def __del__(self):
        pass

    def crawl_one_page(self, url):
        return set(["%s/podstrona" % url])

class Task106Test(unittest.TestCase):
    """Testy do zadania 106."""

    def test(self):
        """Test na kilku URL-ach."""
        crawler = SimpleCrawlerForTest()

        crawler.link_db = {
            "http://www.google.pl" : 0,
            "http://www.wp.pl" : 1,
            "http://www.wyborcza.pl" : 2,
            "http://www.kozaczek.pl" : 0,
        }

        ref_links = set([
            "http://www.google.pl/podstrona",
            "http://www.kozaczek.pl/podstrona"
        ])

        self.failUnlessEqual(ref_links, crawler.crawl_one_level())

if __name__ == '__main__':
    unittest.main()
