#!/bin/zsh

. ./vars

PREFIX="$1"
OPTION="$2"
TASKS_DONE=0
POINTS_AWARDED=0

LOGFILE=`pwd`/log.all.txt
echo '' > "$LOGFILE"

typeset -A POINTS_PER_CLASSES

run_pylint()
{
    T=$1
    DIR=$2
    UNWANTED=$3

    #pylint --disable=R0904,"$UNWANTED" $T ${T/%Test.py/.py} -f parseable | perl -pne "s{^Task}{${PREFIX}${DIR}/Task}" >> $PYLINT_LOG
    pylint --disable=R0904,"$UNWANTED" ${T/%Test.py/.py} -f parseable | perl -pne "s{^Task}{${PREFIX}${DIR}/Task}" >> $PYLINT_LOG
    PYLINT_EXIT_CODE=${pipestatus[1]}
}

run_pylint_single_file()
{
    T=$1
    DIR=$2
    UNWANTED=$3

    pylint --disable=R0904,"$UNWANTED" $T -f parseable | perl -pne "s{^}{${PREFIX}${DIR}/}" >> $PYLINT_LOG
    PYLINT_EXIT_CODE=${pipestatus[1]}
}

process_test()
{
    SOLUTION=${1/%Test.py/.py}
    if [[ -r $SOLUTION ]]
    then
        echo "RUNNING $1"

        JUST_NUMBER=${1/%Test.py/}
        JUST_NUMBER=${JUST_NUMBER/#Task/}

        TASK_KEY="${STUDENT_NUMBER}_${JUST_NUMBER}"

        WAS_OVERRIDDEN=no

        if [[ $OVERRIDDEN_POINTS[$TASK_KEY] != "" ]]
        then
            echo "from overridden.txt: $OVERRIDDEN_POINTS[$TASK_KEY]"
            SUCCESS=yes
            WAS_OVERRIDDEN=yes
            MSS="READ FROM FILE overridden.txt "
        else
            XML_FILE=${1/%.py/.xml}
            nosetests2 --with-xunit --xunit-file="$XML_FILE" $1
            TESTS_OK=$?

            if [[ "$1" == Task10[34567]Test.py ]]
            then
                run_pylint_single_file SimpleCrawler.py $2 "R0201,W0611,W0613"
                PYLINT_OK=$PYLINT_EXIT_CODE
            else
                run_pylint $1 $2
                PYLINT_OK=$PYLINT_EXIT_CODE
            fi

            SUCCESS=yes

            if [[ "$TESTS_OK" == "0" ]]
            then
                MSS="TESTS OK"
            else
                MSS="TESTS FAILED"
                SUCCESS=no
            fi

            if [[ "$PYLINT_OK" == "0" ]]
            then
                MSS="$MSS  PYLINT OK"
            else
                MSS="$MSS  PYLINT FAILED"
                SUCCESS=no
            fi
        fi

        CLASSES=$SOLUTION[-6]
        echo "task belongs to classes $CLASSES"
        CLASSES_TIME_LIMIT=$TIME_LIMITS[$CLASSES]
        echo "classes time limit is $CLASSES_TIME_LIMIT"
        HOME_TIME_LIMIT=$HOME_TIME_LIMITS[$CLASSES]
        echo "home time limit is $HOME_TIME_LIMIT"
        HOME_POINT_LIMIT=$HOME_POINT_LIMITS[$CLASSES]
        echo "home point limit is $HOME_POINT_LIMIT"

        LAST_COMMIT_DATE=`git log -1 --format=%cd --date=iso $SOLUTION`
        echo "last commit to solution is $LAST_COMMIT_DATE"

        AT_HOME="no"
        TASK_POINTS_FOR_HOME=0

        if [[ "$WAS_OVERRIDDEN" == "no" && "$CLASSES_TIME_LIMIT" < "$LAST_COMMIT_DATE" ]]
        then
            if [[ "$HOME_TIME_LIMIT" < "$LAST_COMMIT_DATE" ]]
            then
                MSS="$MSS TOO LATE"
                SUCCESS="no"
                echo "solved too late!"
            else
                MSS="$MSS DONE AT HOME"

                if [[ "$SUCCESS" == "yes" ]]
                then
                    TASK_POINTS_FOR_HOME=1

                    if [[ "$JUST_NUMBER" == "206" ]]
                    then
                        TASK_POINTS_FOR_HOME=4
                    fi

                    HPPC=$HOME_POINTS_PER_CLASSES[$CLASSES]
                    HPPC=`expr $HPPC + $TASK_POINTS_FOR_HOME`

                    HOME_POINT_LIMIT=$HOME_POINT_LIMITS[$CLASSES]

                    if [[ $HPPC -gt $HOME_POINT_LIMIT ]]
                    then
                        TASK_POINTS_FOR_HOME=`expr $HOME_POINT_LIMIT - $HOME_POINTS_PER_CLASSES[$CLASSES]`
                        HPPC=$HOME_POINT_LIMIT
                        MSS="$MSS HOME LIMIT EXCEEDED, AWARDING: $TASK_POINTS_FOR_HOME points"
                    fi

                    MSS="$MSS HOME POINTS: $TASK_POINTS_FOR_HOME"

                    HPOINTS_PER_CLASSES[$CLASSES]=$HPPC
                fi

                AT_HOME="yes"
                echo "solved at home!"
            fi
        fi

        if [[ "$SUCCESS" == "yes" ]]
        then
            TASKS_DONE=`expr $TASKS_DONE + 1`
            TASK_POINTS=`cat $1 | perl -ne 'print "\$1\n" if /^POINTS:\s*(\d+)/'`

            if [[ "$AT_HOME" == "yes" ]]
            then
                TASK_POINTS=$TASK_POINTS_FOR_HOME
            fi

            if [[ $OVERRIDDEN_POINTS[$TASK_KEY] != "" ]]
            then
                TASK_POINTS=$OVERRIDDEN_POINTS[$TASK_KEY]
            fi

            MSS="$MSS POINTS: $TASK_POINTS"

            PPC=$POINTS_PER_CLASSES[$CLASSES]
            PPC=`expr $PPC + $TASK_POINTS`

            POINT_LIMIT=$DEFAULT_POINT_LIMIT

            if [[ $POINT_LIMITS[$CLASSES] != "" ]]
            then
                POINT_LIMIT=$POINT_LIMITS[$CLASSES]
            fi

            if [[ $PPC -gt $POINT_LIMIT ]]
            then
                TASK_POINTS=`expr $POINT_LIMIT - $POINTS_PER_CLASSES[$CLASSES]`
                PPC=$POINT_LIMIT
                MSS="$MSS  LIMIT EXCEEDED, AWARDING: $TASK_POINTS points"
            fi

            echo "$MSS $TASK_POINTS" >> "$LOGFILE"

            POINTS_PER_CLASSES[$CLASSES]=$PPC

            POINTS_AWARDED=`expr $POINTS_AWARDED + $TASK_POINTS`
        fi

        SUMMARY="${SUMMARY}\n$1 $MSS"

    else
        if [[ "$OPTION" == "fullcheck" ]]
        then
            # TODO fix duplication problem
            run_pylint $1 $2 "F0401,E0602,F0001"
        fi
        echo "SKIPPING $1"
        SUMMARY="${SUMMARY}\n$1 NO SOLUTION"
    fi
}

process_directory()
{
    echo "=============================="
    echo "GOING INTO $1"

    pushd $1

    for T in Task*Test.py
    do
        process_test $T $1
    done

    popd
}

read_overrides()
{
    perl -pne 's/\s*\#.*//' < overrides.txt | while read INDEX_NUMBER TASK_CODE POINTS;
    do
        TASK_KEY="${INDEX_NUMBER}_${TASK_CODE}"
        OVERRIDDEN_POINTS[$TASK_KEY]=$POINTS
    done
}

STUDENT_NUMBER=`pwd | perl -ne 'print "$1" if /${PREFIX}-s(\d+)/'`
echo "STUDENT INDEX NUMBER IS: $STUDENT_NUMBER"

typeset -A OVERRIDDEN_POINTS
read_overrides

PYLINT_LOG=`pwd`/pylint.log
rm -f "$PYLINT_LOG"

for D in *(/)
do
    process_directory $D
done

echo "==============================================="
echo "SUMMARY"
echo $SUMMARY

echo ""
echo "TASKS DONE: $TASKS_DONE"
echo "POINTS: $POINTS_AWARDED"
