# -*- coding: utf-8 -*-

"""
Zadanie 605

Napisz funkcję `average_centroids(centroids, clusters, data)`, która
zwraca listę K nowych centroidów na podstawie grup w `clusters`. Każda
grupa składa się z identyfikatorów wektorów (wierszy) z macierzy `data`.
Nowy i-ty centroid obliczamy wektorowo jako sumę wektorów i-tej grupy
(porbranych z macierzy data) podzieloną przez liczbę dokumentów w
i-tej grupie. Uzyskamy w ten sposób uśrednione centroidy. Jeśli i-ta
grupy jest pusta, należy jako nowy i-ty centroid przyjąć poprzedni
i-ty centroid z centroids.

NAME: average_centroids
PARAMS: list, list, function
RETURN: list
POINTS: 3
"""

import unittest
from Task605 import average_centroids

class Task605Test(unittest.TestCase):
    """Testy do zadania 605."""

    def test(self):
        """Prosty test na sztucznej macierzy."""

        matrix = [
            [4.0, 3.0, 0.0, 4.0, 13.0],
            [3.0, 0.0, 1.0, 0.0, 2.0],
            [6.0, 0.0, 0.0, 7.0, 1.0],
            [2.0, 2.0, 1.0, 1.0, 4.0],
            [2.0, 1.0, 0.0, 1.0, 1.0]
        ]

        centroids = [
            [3.2644328743503568, 2.7934728644286571, 0.01658522304574328,
             6.2708699919820141, 12.005606251963332],
            [4.5646558840759699, 2.4540935924358021, 0.8676228638991127,
             6.270117813042738, 3.4442032277680501],
            [5.6932630557785586, 0.89876452517495475, 0.93903459060734684,
             6.9499840123625969, 1.1570764030657847]
        ]

        clusters = [[], [1, 3, 4], [0, 2]]

        centroids1 = average_centroids(centroids, clusters, matrix)
        centroids2 = [
            [3.2644328743503568, 2.7934728644286571, 0.01658522304574328,
             6.2708699919820141, 12.005606251963332],
            [2.3333333333333335, 1.0, 0.66666666666666663,
             0.66666666666666663, 2.3333333333333335],
            [5.0, 1.5, 0.0, 5.5, 7.0]
        ]

        for idx in range(len(centroids1)):
            self.assertEqual(len(centroids1[idx]), len(centroids2[idx]))
            for idy in range(len(centroids1[0])):
                self.assertAlmostEqual(
                    centroids1[idx][idy], centroids2[idx][idy], 3)

if __name__ == '__main__':
    unittest.main()
