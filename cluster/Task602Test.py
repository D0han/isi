# -*- coding: utf-8 -*-

"""
Zadanie 602

Napisz funkcję `pearson(v1, v2)`, która oblicza 1 - r (współczynnik
korelacji Pearsona) dla dwóch podanych wektorów.

NAME: pearson
PARAMS: list, list
RETURN: float
POINTS: 2
"""

import unittest
from Task602 import pearson

class Task602Test(unittest.TestCase):
    """Testy do zadania 602."""

    def test(self):
        """Test seryjny."""
        for vec1, vec2, distance in [
            ([1.4, 2, 3, 4, 5, 6], [1.4, 2, 3, 4, 5, 6], 0.0),
            ([1.4, 2, 3, 4, 5, 6], [1, 2, 3.3, 4, 5.3, 6], 0.0069),
            ([1, -2.3, 3.0, -4, 5, 6], [6, 2, 1, 2, -1, -3], 1.6016),
            ([0, -12, 0, -23], [-1.4, 3.4, -11.2, 2.3], 1.6993),
            ]:
            self.assertAlmostEqual(pearson(vec1, vec2), distance, 2)

if __name__ == '__main__':
    unittest.main()
