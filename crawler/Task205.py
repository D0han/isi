﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 205

Napisz funkcję `overlap_score(simple_index, query, document)`,
 która dla zapytania (lista wyrazów) i zadanego dokumentu 
 oblicza "Overlap Score". Należy skorzystać z rozwiązania 
 zadania 204.

NAME: overlap_score
PARAMS: SimpleIndex, list, string
RETURN: float
POINTS: 2
"""

from Task204 import calc_tf_idf

def overlap_score(simple_index, query, document):
    "opensource jest fajny"
    return sum([calc_tf_idf(simple_index, term, document) for term in query])