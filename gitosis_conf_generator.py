#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
Skrypt generujący wpisy w gitosis.conf dla zadanych na wejściu numerów
indeksu.
"""

import sys
import re

PREFIX=sys.argv[1]

def process_index_number(index_number):
    """
    Główna funkcja generująca wpisy w gitosis.conf dla zadanego numeru indeksu.
    """
    repo_name = "%s-s%s" % (PREFIX, index_number)
    print ""
    print "[group %s]" % repo_name
    print "members = s%s@amu" % index_number
    print "writable = %s" % repo_name
    print ""
    print "[group readonly-%s]" % repo_name
    print "members = @filipg @marcinj @builders"
    print "readonly = %s" % repo_name

for line in sys.stdin:
    line = line.rstrip()
    if not re.match(r"\d+$", line):
        raise BaseException("index number expected, was: %s" % line)
    process_index_number(line)
