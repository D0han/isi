﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Zadanie 203

Napisz funkcję `calc_idf(simple_index, self, term)`, która na
podstawie zawartości indeksu oblicza wartość IDF (odwrotną częstość
dokumentową) dla danego wyrazu. Żeby nie dzielić przez zero, liczbę
dokumentów zawierających dany wyraz należy zwiększyć o 1. Przez to
wartość IDF może przyjąć wartości ujemne w przypadku, gdy wyraz
występuje we wszystkich dokumentach, co jest mało prawdopodobne w
przypadku dużych zbiorów.

NAME: calc_idf
PARAMS: SimpleIndex, string
RETURN: float
POINTS: 3
"""

import math

def calc_idf(simple_index, term):
    "abuse"
    wszystkie = float(len(simple_index.documents))
    simple_index.inverted_index.setdefault(term, [])
    zawierajace = float(len(set(simple_index.inverted_index[term])) +1)
    return math.log(wszystkie/zawierajace, 2)